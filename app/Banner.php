<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = [
        'image',
        'banner_name',
        'heading',
        'description',
        'button',
    ];
}
