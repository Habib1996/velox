<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    protected $fillable = [
        'university_name',
        'vc_name',
        'image',
        'university_moto',
        'university_type',
        'world_ranking',
        'pakistan_ranking',
    ];
}
