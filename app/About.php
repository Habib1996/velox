<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = [
      'heading',
      'sub_heading',
      'image',
      'description',
        'type'
    ];
}
