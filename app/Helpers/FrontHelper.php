<?php
/**
 * Created by PhpStorm.
 * User: Lime Habib
 * Date: 6/15/2018
 * Time: 1:07 PM
 */

namespace App\Helpers;


use App\About;
use App\University;

class FrontHelper
{
    public static function about($type)
    {
        $about = new About();
        if(!empty($type)){
            $about = $about->where(['type' => $type]);
        }
        return $about->first();

    }

    public  static  function university($limit = null, $type = null)
    {
        $university = new University();
        $university = $university->where(['status' => 1]);
        if(!empty($type))
        {
            $university = $university->where(['university_type'=> $type]);
        }
        if(!empty($limit))
        {
            $university = $university->take($limit);
        }
        return $university->orderby('pakistan_ranking','asc')->get();
    }




}