<?php
/**
 * Created by PhpStorm.
 * User: AliPC
 * Date: 5/31/2018
 * Time: 4:29 PM
 */

namespace App\Helpers;

class StatusHelper
{
    public static function getStatus($id)
    {
        $status = '';
        switch ($id){
            case 1:
                $status = "<span class='badge badge-success p-2'>ACTIVE</span>";
                break;
            case 0:
                $status = "<span class='badge badge-danger p-2'>DISABLE</span>";
                break;
            case 2:
                $status = "<span class='badge badge-warning p-2'>PENDING</span>";
                break;
            case 3:
                $status = "<span class='badge badge-success p-2'>APPROVED</span>";
                break;
            case 4:
                $status = "<span class='badge badge-danger p-2'>DELETED</span>";
                break;
            default:
                $status = "<span class='badge badge-success p-2'>ACTIVE</span>";
                break;
        }
        return $status;
    }
}