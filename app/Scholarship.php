<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scholarship extends Model
{
    protected $fillable = [
        'scholarship_name',
        'start_date',
        'last_date',
        'location',
        'eligibility_criteria',
        'terms_conditions',
        'scholarship_type',
        'organization',
        'image',
        'description'
        ];
}
