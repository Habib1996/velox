<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'event_name',
        'start_date',
        'last_date',
        'speaker_name',
        'available_seats',
        'event_type',
        'sponsor_name',
        'description',
        'location',
        'event_fee',
    ];
}
