<?php

namespace App\Mail;

use App\Subscribe;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscribeMail extends Mailable
{
    use Queueable, SerializesModels;

    private $message;

    /**
     * Create a new message instance.
     *
     * @param $request
     * @param $message
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $users = Subscribe::select(['email'])->where('email','!=','')->get();
        $email = [];
        foreach ($users as $user)
        {
            $email[] = $user->email;
        }

        return $this->view('email.contact',['data' => $this->message])->to($email)->subject('New Added');
    }
}
