<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VeloxMail extends Mailable
{
    use Queueable, SerializesModels;

    private $email;
    private $message;
    /**
     * Create a new message instance.
     *
     * @param $request
     */
    public function __construct($request , $message)
    {
        $this->email = $request->email;
        $this->message = $request->message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.contact',['data' => $this->message])->to($this->email)->subject('Velox');
    }
}
