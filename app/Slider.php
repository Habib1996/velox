<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
       'image',
       'slider_name',
       'heading',
       'description',
       'button',
       'status'

    ];

    protected $attributes =[
        'status' => 1
    ];
}


