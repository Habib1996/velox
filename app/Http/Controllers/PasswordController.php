<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

class PasswordController extends Controller
{
    public function index(Request $request)
    {
        if($request->post()){
            $this->validate($request, ['email' => 'required']);
            $user = User::where(['email' => $request->email])->first();
            if(empty($user)){

                return redirect()->route('password-reset');
            }
        }
        return view('auth.passwords.email');
    }
}
