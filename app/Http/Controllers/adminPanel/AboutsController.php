<?php

namespace App\Http\Controllers\adminpanel;

use App\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function status($id)
    {
        $about = About::find($id);
        if($about->status == 1)
        {
            $about->status = 0;
        }
        else
            $about->status = 1;
        $about->update();
        return redirect()->route('about');
    }


    public function index()
    {
        $about = About::orderbydesc('id')->paginate(10);
        return view('adminPanel.about.index', compact('about'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminPanel.about.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $about = About::create([
            'heading' => $request->heading,
            'sub_heading' => $request->sub_heading,
            'description' => $request->description,
            'type' => $request->type,
        ]);
        if (!empty($request->files)) {
            foreach ($request->files as $file) {
                /**
                 * @var $item File
                 */
                foreach ($file as $item) {
                    $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                    $destination = public_path('uploads/abouts');
                    if (!file_exists($destination)) {
                        mkdir($destination, 0777, true);
                    }
                    $item->move($destination, $imageName);
                    $about = About::find($about->id);
                    $about->image = $imageName;
                    $about->update();
                }
            }
        }
        return redirect()->route('about');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $about = About::find($id);
        return view('adminPanel.about.show', compact('about'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = About::find($id);
        return view('adminPanel.about.edit', compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $about = About::find($id);
        if (!empty($about)) {
            $about->heading = $request->heading;
            $about->sub_heading = $request->sub_heading;
            $about->description = $request->description;
            $about->update();
        }
        if (!empty(end($request->files))) {
            if (!empty($about->image)) {
                $path = public_path('uploads/about' . '/' . $about->image);
                if (file_exists($path)) {
                    unlink($path);
                }
            }

            if (!empty($request->files)) {
                foreach ($request->files as $file) {
                    /**
                     * @var $item File
                     */
                    foreach ($file as $item) {
                        $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                        $destination = public_path('uploads/abouts');
                        if (!file_exists($destination)) {
                            mkdir($destination, 0777, true);
                        }
                        $item->move($destination, $imageName);
                        $about->image = $imageName;
                        $about->update();
                    }
                }
            }
            return redirect()->route('about');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about = About::find($id);
        $about->delete();
        return redirect()->route('about');
    }
}
