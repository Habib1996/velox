<?php

namespace App\Http\Controllers\adminPanel;

use App\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Banner::orderbydesc('id')->paginate(10);
        return view('adminPanel.banner.index', compact('banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminPanel.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // to write this function all data will send to database
        $this->validate($request, [
            'banner_name' => 'required',
            'heading' => 'required',
            'button' => 'required',
            'description' => 'required'
        ]);


        $banner =  Banner::create([
           'banner_name' => $request->banner_name,
            'button'=> $request->button,
            'heading'=> $request->heading,
           'description'=> $request->description,
       ]);

        if (!empty($request->files)) {
                foreach ($request->files as $file) {
                    /**
                     * @var $item File
                     */
                    foreach ($file as $item) {
                        $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                        $destination = public_path('uploads/banners');
                        if (!file_exists($destination)) {
                            mkdir($destination, 0777, true);
                        }
                        $item->move($destination, $imageName);
                        $banner = Banner::find($banner->id);
                        $banner->image = $imageName;
                        $banner->update();
                    }
                }
        }


        return redirect()->route('banner');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banner = Banner::find($id);
        return view('adminPanel.banner.show',compact('banner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::find($id);
        return view('adminPanel.banner.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $banner = Banner::find($id);
       if(!empty($banner)){
           $banner->banner_name = $request->banner_name;
           $banner->heading = $request->heading;
           $banner->button = $request->button;
           $banner->description = $request->description;
           $banner->update();
       }


        if (!empty(end($request->files))) {
            if (!empty($banner->image)) {
                $path = public_path('uploads/banner' . '/' . $banner->image);
                if (file_exists($path)) {
                    unlink($path);
                }
            }

            if (!empty($request->files)) {
                foreach ($request->files as $file) {
                    /**
                     * @var $item File
                     */
                    foreach ($file as $item) {
                        $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                        $destination = public_path('uploads/banners');
                        if (!file_exists($destination)) {
                            mkdir($destination, 0777, true);
                        }
                        $item->move($destination, $imageName);
                        $banner->image = $imageName;
                        $banner->update();
                    }
                }
            }
        }
        
        return redirect()->route('banner');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::find($id);
        $banner->delete();
        return redirect()->route('banner');
    }


    public function search(Request $request)
    {
//        return view('adminPanel.banner.search-result');
      
    }
}
