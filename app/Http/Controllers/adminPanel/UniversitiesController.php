<?php

namespace App\Http\Controllers\adminPanel;

use App\Department;
use App\University;
use Hamcrest\Core\DescribedAs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UniversitiesController extends Controller
{
    public function status($id)
    {
        $university = University::find($id);
        if($university->status == 1)
        {
            $university->status = 0;
        }
        else
            $university->status = 1;
        $university->update();
        if($university->university_type == 1)
            return redirect()->route('university-pak');
        elseif($university->university_type ==2)
            return redirect()->route('university-int');
        else
            return redirect()->route('university-college');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pak_index()
    {
      $pakistan = true;
      $university = University::where(['university_type'=> 1])->orderbydesc('id')->paginate(2);
      return view('adminPanel.university.index', compact('pakistan', 'university'));
    }

    public function int_index()
    {
        $international = true;
        $university = University::where(['university_type'=> 2])->orderbydesc('id')->paginate(2);
        return view('adminPanel.university.index', compact('international','university'));
    }

    public function ac_index()
    {
        $university = University::where(['university_type'=> 3])->orderbydesc('id')->paginate(2);
        return view('adminPanel.university.index', compact('university'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pak_create()
    {
        $pakistan = true;
        return view('adminPanel.university.create', compact('pakistan'));
    }

    public function int_create()
    {
        $international = true;
        return view('adminPanel.university.create', compact('international'));
    }

    public function ac_create()
    {
        return view('adminPanel.university.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     $university =   University::create([
           'university_name' => $request->university_name,
           'university_moto' => $request->university_moto,
           'vc_name' => $request->vc_name,
           'university_type' => $request->university_type,
           'world_ranking' => $request->world_ranking,
           'pakistan_ranking' => $request->pakistan_ranking,
       ]);

        if (!empty($request->files)) {
            foreach ($request->files as $file) {
                /**
                 * @var $item File
                 */
                foreach ($file as $item) {
                    $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                    $destination = public_path('uploads/universities');
                    if (!file_exists($destination)) {
                        mkdir($destination, 0777, true);
                    }
                    $item->move($destination, $imageName);
                    $university = University::find($university->id);
                    $university->image = $imageName;
                    $university->update();
                }
            }
        }



       if($request->university_type == 1)
           return redirect()->route('university-pak');
       elseif($request->university_type ==2)
           return redirect()->route('university-int');
       else
           return redirect()->route('university-college');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $university = University::find($id);
        $department = Department::where(['parent'=> $id])->get();
        return view('adminPanel.university.show',compact('university', 'department'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pak_edit($id)
    {
        $university = University::find($id);
        $pakistan = true;
        return view('adminPanel.university.edit', compact('pakistan', 'university'));
    }

    public function int_edit($id)
    {
        $university = University::find($id);
        $international = true;
        return view('adminPanel.university.edit', compact('international', 'university'));
    }

    public function ac_edit($id)
    {
        $college = University::find($id);
        return view('adminPanel.university.edit', compact('college'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $university = University::find($id);

        if(!empty($university)){
            $university->university_name = $request->university_name;
            $university->university_moto = $request->university_moto;
            $university->vc_name = $request->vc_name;
            $university->world_ranking = $request->world_ranking;
            $university->pakistan_ranking = $request->pakistan_ranking;

            $university->update();

            if (!empty(end($request->files))) {
                if (!empty($university->image)) {
                    $path = public_path('uploads/universities' . '/' . $university->image);
                    if (file_exists($path)) {
                        unlink($path);
                    }
                }


                if (!empty($request->files)) {
                    foreach ($request->files as $file) {
                        /**
                         * @var $item File
                         */
                        foreach ($file as $item) {
                            $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                            $destination = public_path('uploads/universities');
                            if (!file_exists($destination)) {
                                mkdir($destination, 0777, true);
                            }
                            $item->move($destination, $imageName);
                            $university = University::find($university->id);
                            $university->image = $imageName;
                            $university->update();
                        }
                    }
                }
            }

        }

        if($request->university_type == 1)
            return redirect()->route('university-pak');
        elseif($request->university_type ==2)
            return redirect()->route('university-int');
        else
            return redirect()->route('university-college');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $university = University::find($id);
        if(!empty($university->image)) {
            if (file_exists(public_path('uploads/universities/' . $university->image))) {
                unlink(public_path('uploads/universities/' . $university->image));
            }
        }
         $university->delete();
        if($university->university_type == 1)
            return redirect()->route('university-pak');
        elseif($university->university_type ==2)
            return redirect()->route('university-int');
        else
            return redirect()->route('university-college');
    }
}
