<?php

namespace App\Http\Controllers\adminPanel;

use App\Scholarship;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScholarshipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pak_index()
    {
        $pakistan = true;
        $scholarships = Scholarship::where(['scholarship_type' => 1])->orderbydesc('id')->paginate(3);
        return view('adminPanel.scholarship.index', compact('pakistan', 'scholarships'));
    }

    public function int_index()
    {
        $international = true;
        $scholarships = Scholarship::where(['scholarship_type' => 2])->orderbydesc('id')->paginate(3);
        return view('adminPanel.scholarship.index',compact('international', 'scholarships'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pak_create()
    {
        $pakistan = true;
        return view('adminPanel.scholarship.create',compact('pakistan'));

    }

    public function int_create()
    {
        $international = true;
        return view('adminPanel.scholarship.create', compact('international'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'scholarship_name' => 'required',
            'eligibility_criteria' => 'required',
            '' => '',
            '' => '',
            '' => '',
            '' => '',
        ]) ;
       $scholarship = Scholarship::create([
           'scholarship_name' =>$request->scholarship_name,
           'eligibility_criteria' =>$request->eligibility_criteria,
           'start_date' => $request->start_date,
           'last_date' => $request->last_date,
           'terms_conditions' => $request->terms_conditions,
           'organization' =>$request->organization,
           'description' => $request->description,
           'scholarship_type' => $request->scholarship_type,
           'location' => $request->location,
       ]);

        if (!empty($request->files)) {
            foreach ($request->files as $file) {
                /**
                 * @var $item File
                 */
                foreach ($file as $item) {
                    $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                    $destination = public_path('uploads/scholarships');
                    if (!file_exists($destination)) {
                        mkdir($destination, 0777, true);
                    }
                    $item->move($destination, $imageName);
                    $scholarship = Scholarship::find($scholarship->id);
                    $scholarship->image = $imageName;
                    $scholarship->update();
                }
            }
        }

        if($request->scholarship_type == 1)
            return redirect(route('scholarships-pak'));
        else
         return redirect(route('scholarship-int'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $scholarship = Scholarship::find($id);
        return view('adminPanel.scholarship.show', compact('scholarship'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function p_edit($id)
    {
        $pakistan = true;
        $scholarship = Scholarship::find($id);
        return view('adminPanel.scholarship.edit', compact('scholarship', 'pakistan'));
    }

    public function i_edit($id)
    {
        $international = true;
        $scholarship = Scholarship::find($id);
        return view('adminPanel.scholarship.edit', compact('scholarship', 'international'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $scholarship = Scholarship::find($id);


        if(!empty($scholarship)){
            $scholarship->eligibility_criteria = $request->eligibility_criteria;
            $scholarship->scholarship_name = $request->scholarship_name;
            $scholarship->start_date = $request->start_date;
            $scholarship->last_date = $request->last_date;
            $scholarship->terms_conditions = $request->terms_conditions;
            $scholarship->organization = $request->organization;
            $scholarship->description = $request->description;
            $scholarship->location = $request->location;


       $scholarship->update();

            if (!empty(end($request->files))) {
                if (!empty($scholarship->image)) {
                    $path = public_path('uploads/scholarship' . '/' . $scholarship->image);
                    if (file_exists($path)) {
                        unlink($path);
                    }
                }

                if (!empty($request->files)) {
                    foreach ($request->files as $file) {
                        /**
                         * @var $item File
                         */
                        foreach ($file as $item) {
                            $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                            $destination = public_path('uploads/scholarships');
                            if (!file_exists($destination)) {
                                mkdir($destination, 0777, true);
                            }
                            $item->move($destination, $imageName);
                            $scholarship->image = $imageName;
                            $scholarship->update();
                        }
                    }
                }
            }
        }

        if ($scholarship->scholarship_type == 1) {
            return redirect(route('scholarships-pak'));
        } else {
            return redirect(route('scholarship-int'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $scholarship = Scholarship::find($id);
        $scholarship->delete();
        if($scholarship->scholarship_type == 2)
            return redirect()->route('scholarship-int');
        else
            return redirect()->route('scholarships-pak');
    }
}
