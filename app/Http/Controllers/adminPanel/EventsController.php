<?php

namespace App\Http\Controllers\adminPanel;

use App\Event;
use App\Mail\SubscribeMail;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventsController extends Controller
{

    public function p_index()
    {
        $pakistan = true;
        $events = Event::where(['event_type' => 1])->orderbydesc('id')->paginate(10);
        return view('adminPanel.events.index', compact('pakistan', 'events'));
    }

    public function i_index()
    {
        $international = true;
        $events = Event::where(['event_type' => 2])->orderbydesc('id')->paginate(10);
        return view('adminPanel.events.index', compact('events', 'international'));
    }


    public function p_create()
    {
        $pakistan = true;

        return view('adminPanel.events.create', compact('pakistan'));
    }

    public function i_create()
    {
        $international = true;
        return view('adminPanel.events.create', compact('international'));
    }


    public function store(Request $request)
    {


        $this->validate($request, [
            'event_name' => 'required',
            'start_date' => 'required',
            'last_date' => 'required',
            'location' => 'required',
            'event_fee' => 'required|integer',
            'available_seats' => 'required',
            'sponsor_name' => 'required',

            'description' => 'required'

        ]);

        $event = Event::create($request->all());
//This is to upload image
        if (!empty($request->files)) {
            foreach ($request->files as $file) {
                /**
                 * @var $item File
                 */
                foreach ($file as $item) {
                    $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                    $destination = public_path('uploads/events');
                    if (!file_exists($destination)) {
                        mkdir($destination, 0777, true);
                    }
                    $item->move($destination, $imageName);
                    $event = Event::find($event->id);
                    $event->image = $imageName;
                    $event->update();
                }
            }
        }
        \Mail::send(new SubscribeMail('New Event Created'));
        if ($request->event_type == 1) {
            return redirect(route('event-pak'));
        } else {
            return redirect(route('event-int'));
        }
    }

    public function show($id)
    {
        $event = Event::find($id);
        return view('adminPanel.events.show', compact('event'));
    }


    public function p_edit($id)
    {
        $pakistan = true;
        $event = Event::find($id);
        return view('adminPanel.events.edit', compact('event', 'pakistan'));
    }

    public function i_edit($id)
    {
        $international = true;
        $event = Event::find($id);
        return view('adminPanel.events.edit', compact('event', 'international'));
    }

    public function update(Request $request, $id)
    {
        $event = Event::find($id);

        if (!empty($event)) {

            $event->event_name = $request->event_name;
            $event->last_date = $request->last_date;
            $event->start_date = $request->start_date;
            $event->location = $request->location;
            $event->event_fee = $request->event_fee;
            $event->available_seats = $request->available_seats;
            $event->sponsor_name = $request->sponsor_name;
            $event->description = $request->description;
            $event->update();


            if (!empty(end($request->files))) {
                if (!empty($event->image)) {
                    $path = public_path('uploads/events' . '/' . $event->image);
                    if (file_exists($path)) {
                        unlink($path);
                    }
                }


                if (!empty($request->files)) {
                    foreach ($request->files as $file) {
                        /**
                         * @var $item File
                         */
                        foreach ($file as $item) {
                            $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                            $destination = public_path('uploads/events');
                            if (!file_exists($destination)) {
                                mkdir($destination, 0777, true);
                            }
                            $item->move($destination, $imageName);
                            $event = Event::find($event->id);
                            $event->image = $imageName;
                            $event->update();
                        }
                    }
                }
            }
            if ($event->event_type == 1) {
                return redirect(route('event-pak'));
            } else {
                return redirect(route('event-int'));
            }
        }
    }

    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete();

        if ($event->event_type == 1) {
            return redirect(route('scholarships-pak'));
        } else {
            return redirect(route('scholarships-int'));
        }
    }
}
