<?php

namespace App\Http\Controllers\adminPanel;

use App\Department;
use App\University;
use Hamcrest\Core\DescribedAs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parent = null)
    {

        return view('adminPanel.departments.create',compact('parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'department_name' => 'required',
            'description' => 'required',
            'image' => 'required',

        ]);

        $department = Department::create([
            'name' => $request->department_name,
            'description' => $request->description,
            'parent' => $request->parent,
        ]);

        if (!empty($request->files)) {
            foreach ($request->files as $file) {
                /**
                 * @var $item File
                 */
                foreach ($file as $item) {
                    $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                    $destination = public_path('uploads/department');
                    if (!file_exists($destination)) {
                        mkdir($destination, 0777, true);
                    }
                    $item->move($destination, $imageName);
                    $department = Department::find($department->id);
                    $department->image = $imageName;
                    $department->update();
                }
            }
        }
        if($request->university_type == 1)
        return redirect()->route('university-pakistan-show', $request->parent);
        else if ($request->university_type == 2)
            return redirect()->route('university-international-show', $request->parent);
        else
            return redirect()->route('university-college-show', $request->parent);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = Department::find($id);
        return view('adminPanel.university.department_show', compact('department'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::find($id);
        $university = University::where(['id'=> $department->parent])->first();  //find data against id   and first is used to find single record
        return view('adminPanel.university.department_edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $department = Department::find($id);
        if (!empty($department)) {


            $department->name = $request->name;
            $department->description = $request->description;
            $department->update();

            if (!empty(end($request->files))) {
                if (!empty($department->image)) {
                    $path = public_path('uploads/department' . '/' . $department->image);
                    if (file_exists($path)) {
                        unlink($path);
                    }
                }

                if (!empty($request->files)) {
                    foreach ($request->files as $file) {
                        /**
                         * @var $item File
                         */
                        foreach ($file as $item) {
                            $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                            $destination = public_path('uploads/department');
                            if (!file_exists($destination)) {
                                mkdir($destination, 0777, true);
                            }
                            $item->move($destination, $imageName);
                            $department->image = $imageName;
                            $department->update();
                        }
                    }
                }
            }

        }

        if($request->university_type == 1)
            return redirect()->route('university-pakistan-show', $request->parent);
        else if ($request->university_type == 2)
            return redirect()->route('university-international-show', $request->parent);
        else
            return redirect()->route('university-college-show', $request->parent);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = Department::find($id);
        if(file_exists(public_path('uploads/department/'.$department->image))){
            unlink(public_path('uploads/department/'.$department->image));
        }
        $department->delete();
        return redirect()->route('university-show', $department->parent);
    }
}
