<?php

namespace App\Http\Controllers\adminPanel;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use SebastianBergmann\CodeCoverage\Node\File;

class SlidersController extends Controller
{

    public function status($id)
    {
        $slider = Slider::find($id);
        if($slider->status == 1)
        {
            $slider->status = 0;
        }
        else
            $slider->status = 1;
        $slider->update();
        return redirect()->route('slider');
    }

    public function index()
    {
        $sliders = Slider::orderbydesc('id')->paginate(10);
        return view('adminPanel.slider.index', compact('sliders'));
    }

    public function create()
    {
        return view('adminPanel.slider.create');

    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'slider_name' => 'required',
            'heading' => 'required',
            'button' => 'required',
            'description' => 'required'
        ]);

        $Slider = Slider::create([
            'slider_name' => $request->slider_name,
            'heading' => $request->heading,
            'button' => $request->button,
            'description' => $request->description,
        ]);

        if (!empty($request->files)) {
            foreach ($request->files as $file) {
                /**
                 * @var $item File
                 */
                foreach ($file as $item) {
                    $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                    $destination = public_path('uploads/sliders');
                    if (!file_exists($destination)) {
                        mkdir($destination, 0777, true);
                    }
                    $item->move($destination, $imageName);
                    $Slider = Slider::find($Slider->id);
                    $Slider->image = $imageName;
                    $Slider->update();
                }
            }
        }
        return redirect()->route('slider');
    }

    public function show($id)
    {
        $slider = Slider::find($id);
        return view('adminPanel.slider.show', compact('slider'));
    }

    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('adminPanel.slider.edit', compact('slider'));

    }


    public function update(Request $request, $id)
    {
        $slider = Slider::find($id);
        if (!empty($slider)) {


            $slider->slider_name = $request->slider_name;
            $slider->heading = $request->heading;
            $slider->button = $request->button;
            $slider->description = $request->description;
            $slider->update();

            if (!empty(end($request->files))) {
                if (!empty($slider->image)) {
                    $path = public_path('uploads/slider' . '/' . $slider->image);
                    if (file_exists($path)) {
                        unlink($path);
                    }
                }

                if (!empty($request->files)) {
                    foreach ($request->files as $file) {
                        /**
                         * @var $item File
                         */
                        foreach ($file as $item) {
                            $imageName = date('y-m-d') . uniqid() . '.' . $item->getClientOriginalExtension();  //this is used to store file extension and unique is used to change file name

                            $destination = public_path('uploads/sliders');
                            if (!file_exists($destination)) {
                                mkdir($destination, 0777, true);
                            }
                            $item->move($destination, $imageName);
                            $slider->image = $imageName;
                            $slider->update();
                        }
                    }
                }
            }

        }


        return redirect()->route('slider');
    }


    public function destroy($id)
    {
        $slider = Slider::find($id);
        if(file_exists(public_path('uploads/slider/'.$slider->image))){
            unlink(public_path('uploads/slider/'.$slider->image));
        }
        $slider->delete();
        return redirect()->route('slider');
    }
}
