<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
      'name',
      'status_id',
      'description',
        'image',
        'parent'
    ];
}
