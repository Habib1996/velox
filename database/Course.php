<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
      'course_name',
      'start_date',
      'last_date',
      'learning_outcomes',
      'lectures',
      'features',
      'Instructor',
      'quizzes',
      'duration',
      'skill_level',
      'languages',
      'students',
      'image',
      'description',
    ];
}
