<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id');
            $table->string('course_name');
            $table->string('start_date');
            $table->string('last_date');
            $table->string('learning_outcomes');
            $table->string('lectures');
            $table->string('features');    // certificate information will be enter here
            $table->string('Instructor');
            $table->string('quizzes');
            $table->string('duration');
            $table->string('skill_level');
            $table->string('languages');
            $table->string('students');
            $table->string('image');
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
