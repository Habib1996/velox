<section>
    <div class="lgx-banner lgx-banner-six">
        <div class="lgx-banner-style">
            <div class="lgx-inner lgx-inner-fixed">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="lgx-banner-info  lgx-banner-info-white">
                                <h3 class="subtitle">VELOX</h3>
                                <h2 class="title">All Courses</h2>
                                <strong> <p class="text">Velox is the best website for the students who want to get Higher Education
                                        <br/> its provide the information about Universities, Courses, Events and Scholarships
                                        <br> in Pakistan and all around the world</p></strong>

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="lgx-video-area-bg">
                                <figure>
                                    <a href="#"><img src="{{asset('assets/views/assets/img/video.jpg')}}" alt="Special Food"></a>
                                    <figcaption>
                                        <div class="video-icon">
                                            <div class="lgx-vertical">
                                                <a id="myModalLabel" class="icon" href="#" data-toggle="modal" data-target="#lgx-modal">
                                                    <i class="fa fa-play" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                                <!-- Modal-->
                                <div id="lgx-modal" class="modal fade lgx-modal">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <iframe id="modalvideo" src="https://www.youtube.com/embed/GJW2i5urzVk" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- //.Modal-->
                            </div>
                        </div>
                    </div>
                    <!--//.ROW-->
                </div>
                <!-- //.CONTAINER -->
            </div>
            <!-- //.INNER -->
        </div>
    </div>
</section>
