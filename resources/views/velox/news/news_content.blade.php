
<!--BLOG -->
<section>
    <div id="lgx-blog" class="lgx-blog lgx-blog-normal">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">

                   
                    <div class="lgx-news-single">
                        <figure>
                            <img src="{{ asset('assets/views/assets/img/news/news2.jpg') }}" alt="Adv" title="Adv"/>
                            <figcaption>
                                <div class="figcaption">
                                    <div class="lgx-hover-link">
                                        <div class="lgx-vertical">
                                            <a href="news-single.html"><i class="fa fa-book"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="author">
                                    <div class="author-info">
                                        <img src="{{ asset('assets/views/assets/img/news/author2.jpg') }}" alt="author">
                                        <div class="author-info">
                                            <h4 class="title"><a href="#">Riazul Islam</a></h4>
                                            <h5 class="subtitle">Head of Education</h5>
                                        </div>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                        <div class="text-area">
                            <h3 class="title"><a href="news-single.html">UX Education: Designing free Online Learning Curriculum</a></h3>
                            <p class="text">Lorem ipsum dolor sit amet, consectetuer adir elit. Aenean commodo ligula eget ...</p>
                            <div class="hits-area">
                                <span class="date"></span>
                            </div>
                            <div class="text-bottom">
                                <a class="date" href="#">25 July 2016</a>
                                <a class="link" href="news-single.html"><i class="fa  fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div> <!--//.News-single-->
                    <!--News-single-->
                    <div class="lgx-news-single">
                        <figure>
                            <img src="{{ asset('assets/views/assets/img/news/news3.jpg') }}" alt="Adv" title="Adv"/>
                            <figcaption>
                                <div class="figcaption">
                                    <div class="lgx-hover-link">
                                        <div class="lgx-vertical">
                                            <a href="news-single.html"><i class="fa fa-book"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="author">
                                    <div class="author-info">
                                        <img src="{{ asset('assets/views/assets/img/news/author3.jpg') }}" alt="author">
                                        <div class="author-info">
                                            <h4 class="title"><a href="#">Jewel Jonathon</a></h4>
                                            <h5 class="subtitle">Professor of Education</h5>
                                        </div>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                        <div class="text-area">
                            <h3 class="title"><a href="news-single.html">The digital revolution in higher education has already happened</a></h3>
                            <p class="text">Lorem ipsum dolor sit amet, consectetuer adir elit. Aenean commodo ligula eget ...</p>
                            <div class="hits-area">
                                <span class="date"></span>
                            </div>
                            <div class="text-bottom">
                                <a class="date" href="#">25 July 2016</a>
                                <a class="link" href="news-single.html"><i class="fa  fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div> <!--//.News-single-->
                  
                  
                    <div class="lgx-news-single">
                        <figure>
                            <img src="{{ asset('assets/views/assets/img/news/news4.jpg') }}" alt="Adv" title="Adv"/>
                            <figcaption>
                                <div class="figcaption">
                                    <div class="lgx-hover-link">
                                        <div class="lgx-vertical">
                                            <a href="news-single.html"><i class="fa fa-book"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="author">
                                    <div class="author-info">
                                        <img src="{{ asset('assets/views/assets/img/news/author4.jpg') }}" alt="author">
                                        <div class="author-info">
                                            <h4 class="title"><a href="#">Devid Talukdar</a></h4>
                                            <h5 class="subtitle">Master of Education</h5>
                                        </div>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                        <div class="text-area">
                            <h3 class="title"><a href="news-single.html">I no longer understand my PhD dissertation on Education</a></h3>
                            <p class="text">Lorem ipsum dolor amet, consectetuer adir elit. Aenean commodo ligula dolor ...</p>
                            <div class="hits-area">
                                <span class="date"></span>
                            </div>
                            <div class="text-bottom">
                                <a class="date" href="#">25 July 2016</a>
                                <a class="link" href="news-single.html"><i class="fa  fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div> <!--//.News-single-->

                    <!--News-single-->
                    <div class="lgx-news-single">
                        <figure>
                            <img src="{{ asset('assets/views/assets/img/news/news2.jpg') }}" alt="Adv" title="Adv"/>
                            <figcaption>
                                <div class="figcaption">
                                    <div class="lgx-hover-link">
                                        <div class="lgx-vertical">
                                            <a href="news-single.html"><i class="fa fa-book"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="author">
                                    <div class="author-info">
                                        <img src="{{ asset('assets/views/assets/img/news/author2.jpg') }}" alt="author">
                                        <div class="author-info">
                                            <h4 class="title"><a href="#">Riazul Islam</a></h4>
                                            <h5 class="subtitle">Head of Education</h5>
                                        </div>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                        <div class="text-area">
                            <h3 class="title"><a href="news-single.html">UX Education: Designing free Online Learning Curriculum</a></h3>
                            <p class="text">Lorem ipsum dolor sit amet, consectetuer adir elit. Aenean commodo ligula eget ...</p>
                            <div class="hits-area">
                                <span class="date"></span>
                            </div>
                            <div class="text-bottom">
                                <a class="date" href="#">25 July 2016</a>
                                <a class="link" href="news-single.html"><i class="fa  fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div> <!--//.News-single-->
                    <!--News-single-->
                    <div class="lgx-news-single">
                        <figure>
                            <img src="{{ asset('assets/views/assets/img/news/news3.jpg') }}" alt="Adv" title="Adv"/>
                            <figcaption>
                                <div class="figcaption">
                                    <div class="lgx-hover-link">
                                        <div class="lgx-vertical">
                                            <a href="news-single.html"><i class="fa fa-book"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="author">
                                    <div class="author-info">
                                        <img src="{{ asset('assets/views/assets/img/news/author3.jpg') }}" alt="author">
                                        <div class="author-info">
                                            <h4 class="title"><a href="#">Jewel Jonathon</a></h4>
                                            <h5 class="subtitle">Professor of Education</h5>
                                        </div>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                        <div class="text-area">
                            <h3 class="title"><a href="news-single.html">The digital revolution in higher education has already happened</a></h3>
                            <p class="text">Lorem ipsum dolor sit amet, consectetuer adir elit. Aenean commodo ligula eget ...</p>
                            <div class="hits-area">
                                <span class="date"></span>
                            </div>
                            <div class="text-bottom">
                                <a class="date" href="#">25 July 2016</a>
                                <a class="link" href="news-single.html"><i class="fa  fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div> <!--//.News-single-->
                    <!--News-single-->
                    <div class="lgx-news-single">
                        <figure>
                            <img src="{{ asset('assets/views/assets/img/news/news4.jpg') }}" alt="Adv" title="Adv"/>
                            <figcaption>
                                <div class="figcaption">
                                    <div class="lgx-hover-link">
                                        <div class="lgx-vertical">
                                            <a href="news-single.html"><i class="fa fa-book"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="author">
                                    <div class="author-info">
                                        <img src="{{ asset('assets/views/assets/img/news/author4.jpg') }}" alt="author">
                                        <div class="author-info">
                                            <h4 class="title"><a href="#">Devid Talukdar</a></h4>
                                            <h5 class="subtitle">Master of Education</h5>
                                        </div>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                        <div class="text-area">
                            <h3 class="title"><a href="news-single.html">I no longer understand my PhD dissertation on Education</a></h3>
                            <p class="text">Lorem ipsum dolor amet, consectetuer adir elit. Aenean commodo ligula dolor ...</p>
                            <div class="hits-area">
                                <span class="date"></span>
                            </div>
                            <div class="text-bottom">
                                <a class="date" href="#">25 July 2016</a>
                                <a class="link" href="news-single.html"><i class="fa  fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div> <!--//.News-single-->

                </div>
            </div><!-- //.CONTAINER -->
        </div><!-- //.INNER -->
    </div>
</section>
<!--BLOG END-->
