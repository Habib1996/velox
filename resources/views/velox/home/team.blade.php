
<!--TEACHERS-->
<section>
    <div id="lgx-teachers" class="lgx-teachers">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-heading">
                            <h2 class="heading-title">Qualified Teachers</h2>
                            <h4 class="heading-subtitle">Some Special Teachers From The Industry!</h4>
                        </div>
                    </div>
                </div>
                <!--//.ROW-->
                <div class="row">
                    <div id="lgx-owlteachers" class="owl-carousel lgx-owlteachers">

                        <div class="item">
                            <div class="lgx-single-teacher"> <!--lgx-teacher-circle-->
                                <figure>
                                    <a href="teacher-single.html"><img src="{{ asset('assets/views/assets/img/teachers/teacher1.jpg') }}" alt="teacher"/></a>
                                    <figcaption>
                                        <h3 class="teacher-name"><a href="teacher-single.html">Enathon Jackson</a><span>Lecturer</span></h3>
                                        <p class="text">Integer sollicitudin molestie ante etme natoque penatibus et magnie nasceur nulla eleifend ollicitudin molestie</p>
                                        <div class="teacher-bottom">
                                            <ul class="list-inline social-group">
                                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            </ul>
                                            <a class="link" href="teacher-single.html"><i class="fa  fa-long-arrow-right" aria-hidden="true"></i></a>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div> <!--//.Item-->
                        <div class="item">
                            <div class="lgx-single-teacher"> <!--lgx-teacher-circle-->
                                <figure>
                                    <a href="teacher-single.html"><img src="{{ asset('assets/views/assets/img/teachers/teacher2.jpg') }}" alt="teacher"/></a>
                                    <figcaption>
                                        <h3 class="teacher-name"><a href="teacher-single.html">Enathon Jackson</a><span>Lecturer</span></h3>
                                        <p class="text">Integer sollicitudin molestie ante etme natoque penatibus et magnie nasceur nulla eleifend ollicitudin molestie</p>
                                        <div class="teacher-bottom">
                                            <ul class="list-inline social-group">
                                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            </ul>
                                            <a class="link" href="teacher-single.html"><i class="fa  fa-long-arrow-right" aria-hidden="true"></i></a>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div> <!--//.Item-->
                        <div class="item">
                            <div class="lgx-single-teacher">
                                <figure>
                                    <a href="teacher-single.html"><img src="{{ asset('assets/views/assets/img/teachers/teacher3.jpg') }}" alt="teacher"/></a>
                                    <figcaption>
                                        <h3 class="teacher-name"><a href="teacher-single.html">Enathon Jackson</a><span>Lecturer</span></h3>
                                        <p class="text">Integer sollicitudin molestie ante etme natoque penatibus et magnie nasceur nulla eleifend ollicitudin molestie</p>
                                        <div class="teacher-bottom">
                                            <ul class="list-inline social-group">
                                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            </ul>
                                            <a class="link" href="teacher-single.html"><i class="fa  fa-long-arrow-right" aria-hidden="true"></i></a>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div> <!--//.Item-->
                        <div class="item">
                            <div class="lgx-single-teacher">
                                <figure>
                                    <a href="teacher-single.html"><img src="{{ asset('assets/views/assets/img/teachers/teacher2.jpg') }}" alt="teacher"/></a>
                                    <figcaption>
                                        <h3 class="teacher-name"><a href="teacher-single.html">Enathon Jackson</a><span>Lecturer</span></h3>
                                        <p class="text">Integer sollicitudin molestie ante etme natoque penatibus et magnie nasceur nulla eleifend ollicitudin molestie</p>
                                        <div class="teacher-bottom">
                                            <ul class="list-inline social-group">
                                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            </ul>
                                            <a class="link" href="teacher-single.html"><i class="fa  fa-long-arrow-right" aria-hidden="true"></i></a>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div> <!--//.Item-->
                    </div><!--l//#lgx-OWL NEWS-->
                </div>
            </div>
            <!-- //.CONTAINER -->
        </div>
        <!-- //.INNER -->
    </div>
</section>
<!--TEACHERS END-->