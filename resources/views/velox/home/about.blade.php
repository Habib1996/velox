
<?php

$about = \App\Helpers\FrontHelper::about(0);


?>


<section>
    <div id="lgx-about" class="lgx-about lgx-about-bottom"> <!--lgx-about-bottom-->
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                        <div class="lgx-about-area">
                            <div class="lgx-heading">
                                <h2 class="heading-title">{{$about->heading}}</h2>
                                <h4 class="heading-subtitle">Pellentesque habitant morbi tristique senectus et netus et malesuada habitant morbi tristique senectus netus fames ac turpis.</h4>
                            </div>
                            <div class="lgx-about-content">
                                <p class="text">
                                    Pellentesque habitant morbi tristique senectus netus et malesuada fames turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris Eonec eu ribero sit amet quam egestas semper. Aenean are ultricies mi vitae est tristique senectus et netus et malesuada placerat leo.
                                </p>
                                <a class="lgx-btn lgx-btn-sm lgx-btn-border" href="about.html">Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <img src="{{asset('uploads/abouts/'.$about->image)}}" alt="about">
                    </div>
                </div>

                <!--//.ROW-->
            </div>
            <!-- //.CONTAINER -->
        </div>
        <!-- //.INNER -->
    </div>
</section>
