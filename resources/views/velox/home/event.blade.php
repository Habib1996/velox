
<section>
        <div id="lgx-events" class="lgx-events">
            <div class="lgx-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading">
                                <h2 class="heading-title">Upcoming Events</h2>
                                <h4 class="heading-subtitle">Our Upcoming Seminars You Dont Miss Out!</h4>
                            </div>
                        </div>
                    </div>
                    <!--//.ROW-->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-events-area">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="lgx-featured-event">
                                            <figure>
                                                <a href="#"><img src="{{asset('assets/views/assets/img/events/event1.jpg')}}" alt="featured event"></a>
                                                <figcaption>
                                                    <div class="figcaption">
                                                    </div>
                                                    <div class="event-info">
                                                        <div class="date">
                                                            <h4>12<span>jan</span></h4>
                                                        </div>
                                                        <div class="info-right">
                                                            <p>6:30 AM - 5:00 PM</p>
                                                            <h4 class="location"> Swick Hill Street, Charlotte, UK</h4>
                                                            <h3 class="title">
                                                                <a href="event-single.html">The 10 best educational websites apps to learn new stuff for free</a>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="lgx-single-event">
                                            <div class="thumb">
                                                <a href="event-single.html"><img src="{{asset('assets/views/assets/img/events/event2.jpg')}}" alt="event"></a>
                                            </div>
                                            <div class="event-info">
                                                <a class="date" href="#">June 14, 2017</a>
                                                <h4 class="location"> Swick Hill Street, Charlotte, UK</h4>
                                                <h3 class="title"><a href="event-single.html">I no longer understand my PhD dissertation on Education</a></h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="lgx-single-event">
                                            <div class="thumb">
                                                <a href="event-single.html"><img src="{{asset('assets/views/assets/img/events/event3.jpg')}}" alt="event"></a>
                                            </div>
                                            <div class="event-info">
                                                <a class="date" href="#">December 24, 2016</a>
                                                <h4 class="location"> Swick Hill Street, Charlotte, UK</h4>
                                                <h3 class="title"><a href="event-single.html">The digital revolution in higher education has already happened</a></h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="lgx-single-event">
                                            <div class="thumb">
                                                <a href="event-single.html"><img src="{{asset('assets/views/assets/img/events/event4.jpg')}}" alt="event"></a>
                                            </div>
                                            <div class="event-info">
                                                <a class="date" href="#">March 30, 2015</a>
                                                <h4 class="location"> Swick Hill Street, Charlotte, UK</h4>
                                                <h3 class="title"><a href="event-single.html">UX Education: Designing free Online Learning Curriculum</a></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//.ROW-->
                </div>
                <!-- //.CONTAINER -->
            </div>
            <!-- //.INNER -->
        </div>
    </section>
