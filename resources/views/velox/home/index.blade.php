<!doctype html>
<html class="no-js" lang="en">

<head>
    @include('velox.layouts.header')
</head>
<body class="home">

<div class="lgx-container ">

<!--HEADER-->
@include('velox.layouts.nav_bar')
<!--HEADER END-->

<!--HEADER-->
@include('velox.home.slider')
<!--HEADER END-->

<!--ABOUT-->
@include('velox.home.about')
<!--ABOUT END-->

<!--UNIVERSITY-->
@include('velox.home.university')
<!--UNIVERSITY END-->

<!--USER HELP-->
@include('velox.home.user_help')
<!--USER HELP END-->

<!--COURSE HELP-->
@include('velox.home.course')
<!--COURSE HELP END-->

<!--SERVICES HELP-->
@include('velox.home.service')
<!--SERVICES END-->

<!--EVENTS-->
@include('velox.home.event')
<!--EVENTS END-->

<!--NEWS HELP-->
@include('velox.home.news')
<!--NEWS END-->

<!--FOOTER-->
 @include('velox.layouts.footer')
<!--FOOTER END-->

</div>

 @include('velox.layouts.footerfiles')

</body>

<!-- Mirrored from themearth.com/demo/html/educationplus/view/home-slider.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Feb 2018 12:37:12 GMT -->
</html>


