<section>
    <div id="lgx-register" class="lgx-register">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <div class="lgx-registration-area">
                            <div class="lgx-heading-registration">
                                <h3 class="subtitle">You’re Learning Free</h3>
                                <h2 class="title">Started Today!</h2>
                            </div>
                            <div class="lgx-registration-info">
                                <p class="text">Mauris sagittis felis vitae augue posuere fringilla. Nulla tincidunt, magna non sceleris tempor, libero arcu tempor erat, quis imperdiet mi purus eget arcu. Sed luctus viverra libero, non rutrum ligula volutpat a. Etiam commodo bibendum elit at vestibulum</p>
                                <a class="lgx-btn registration-btn" href="registration.html">Registration</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="lgx-video-area">
                            <figure>
                                <figcaption>
                                    <div class="video-icon">
                                        <div class="lgx-vertical">
                                            <span>Watch</span>
                                            <a id="myModalLabel" class="icon" href="#" data-toggle="modal" data-target="#lgx-modal">
                                                <i class="fa fa-play " aria-hidden="true"></i>
                                            </a>
                                            <span>Video</span>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <!-- Modal-->
                            <div id="lgx-modal" class="modal fade lgx-modal">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <iframe id="modalvideo" src="https://www.youtube.com/embed/GJW2i5urzVk" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- //.Modal-->
                        </div>
                    </div>
                </div>
            </div><!-- //.CONTAINER -->
        </div><!-- //.INNER -->
    </div>
</section>
