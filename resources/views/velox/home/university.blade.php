<?php
    $universities  = \App\Helpers\FrontHelper::university(4);
?>
<section>
    <div id="lgx-courses" class="lgx-courses">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-heading">
                            <h2 class="heading-title">University</h2>
                            <h4 class="heading-subtitle">HEC Ranking Wise Universities & Colleges</h4>
                        </div>
                    </div>
                </div>
                <!--//.ROW-->
                <div class="lgx-tab">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-filter-area">

                            </div>
                        </div>
                    </div><!--//. ROW-->

                    @foreach($universities as $university)
                    <div class="lgx-grid-item col-xs-12 col-sm-6 col-md-3 html">
                              <div class="lgx-single-course">
                                  <div class="lgx-single-course-inner">
                                      <figure>
                                          <img src="{{asset('uploads/universities/'.$university->image)}}" alt="course">
                                          <figcaption>
                                              <div class="lgx-hover-link">
                                                  <div class="lgx-vertical">
                                                      <a href="course-single.html">
                                                          <i class="fa fa-book"></i>
                                                      </a>
                                                  </div>
                                              </div>
                                          </figcaption>
                                      </figure>
                                      <div class="course-info">
                                          <div class="course-author">
                                              <!-- {{-- <img src="{{asset('assets/views/assets/img/news/author2.jpg')}}" alt="course"> --}} -->
                                              <div class="author-info top_margin">
                                                  <h4 class="title"><a href="#">{{$university->university_name}}</a></h4>
                                                  <h5 class="subtitle">Prof. Dr. Muhammad Zakria Zakar</h5>
                                              </div>
                                          </div>
                                          <h3 class="title"><a href="course-single.html"> PU is a Public   </a></h3>
                                          <div class="course-bottom">
                                              <ul class="list-inline">
                                                  <li><a href="#"><span>World Ranking</span></i> 1671</a></li>

                                                  <li><a href="#"><span>Country Ranking</span> 1 </a></li>
                                              </ul>

                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div><!--//ITEM-->
                    @endforeach





                                <div class="row">
                                    <div class="col-xs-12">
                                        <h3 class="lgx-getintouch">

                                            <a class="lgx-btn lgx-btn-contact rippler rippler-default" href="{{route('velox-university')}}">View all Universities</a>
                                        </h3>
                                    </div>
                                </div>


                </div>
            </div>
        </div>
    </div>
</section>
