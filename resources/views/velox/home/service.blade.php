<section>
    <div id="lgx-services" class="lgx-services"> <!--lgx-services-white-->
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="lgx-single-service"> <!--lgx-single-service-white-->
                            <span class="icon"><i class="fa fa-play-circle-o" aria-hidden="true"></i></span>
                            <h2 class="title"><a href="#">Free Tutorials</a></h2>
                            <p>Etiam vel ante ac lacus vestibulum rutrum. Aliquam vehicula, massa in auctor dapibus commodo quis vehicula lacus metus sed justo. </p>
                            <a class="lgx-btn-simple" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="lgx-single-service"> <!--lgx-single-service-white-->
                            <span class="icon"><i class="fa fa-address-book" aria-hidden="true"></i></span>
                            <h2 class="title"><a href="#">500+ Courses</a></h2>
                            <p>Etiam vel ante ac lacus vestibulum rutrum. Aliquam vehicula, massa in auctor dapibus commodo quis vehicula lacus metus sed justo. </p>
                            <a class="lgx-btn-simple" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="lgx-single-service"> <!--lgx-single-service-white-->
                            <span class="icon"><i class="fa fa-book" aria-hidden="true"></i></span>
                            <h2 class="title"><a href="#">180k Books Available</a></h2>
                            <p>Etiam vel ante ac lacus vestibulum rutrum. Aliquam vehicula, massa in auctor dapibus commodo quis vehicula lacus metus sed justo. </p>
                            <a class="lgx-btn-simple" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="lgx-single-service"> <!--lgx-single-service-white-->
                            <span class="icon"><i class="fa fa-graduation-cap" aria-hidden="true"></i></span>
                            <h2 class="title"><a href="#">Certified teachers</a></h2>
                            <p>Etiam vel ante ac lacus vestibulum rutrum. Aliquam vehicula, massa in auctor dapibus commodo quis vehicula lacus metus sed justo. </p>
                            <a class="lgx-btn-simple" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="lgx-single-service"> <!--lgx-single-service-white-->
                            <span class="icon"><i class="fa fa-certificate" aria-hidden="true"></i></span>
                            <h2 class="title"><a href="#">Certification</a></h2>
                            <p>Etiam vel ante ac lacus vestibulum rutrum. Aliquam vehicula, massa in auctor dapibus commodo quis vehicula lacus metus sed justo. </p>
                            <a class="lgx-btn-simple" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="lgx-single-service"> <!--lgx-single-service-white-->
                            <span class="icon"><i class="fa fa-language" aria-hidden="true"></i></span>
                            <h2 class="title"><a href="#">Language Lessons</a></h2>
                            <p>Etiam vel ante ac lacus vestibulum rutrum. Aliquam vehicula, massa in auctor dapibus commodo quis vehicula lacus metus sed justo. </p>
                            <a class="lgx-btn-simple" href="#">Read More</a>
                        </div>
                    </div>
                </div>
            </div><!-- //.CONTAINER -->
        </div><!-- //.INNER -->
    </div>
</section>
