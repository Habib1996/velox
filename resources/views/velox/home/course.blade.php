
<section>
    <div id="lgx-courses" class="lgx-courses">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-heading">
                            <h2 class="heading-title">Colleges</h2>
                            <div class="wrap">

                                <h4 class="heading-subtitle">HEC Ranking Wise Colleges</h4>
                            </div>
                        </div>
                    </div>
                    <!--//.ROW-->
                    <div class="lgx-tab">
                        <div class="row">
                            <div class="col-xs-12">

                            </div>
                        </div><!--//. ROW-->

                        <div class="row">
                            <div id="lgx-grid-wrapper" class="lgx-grid-wrapper"> <!--lgx-list-wrapper-->

                                <div class="lgx-grid-item col-xs-12 col-sm-6 col-md-3 html">
                                    <div class="lgx-single-course">
                                        <div class="lgx-single-course-inner">
                                            <figure>
                                                <img src="{{asset('assets/views/assets/img/univerities/pu.jpg')}}" alt="course">
                                                <figcaption>
                                                    <div class="lgx-hover-link">
                                                        <div class="lgx-vertical">
                                                            <a href="course-single.html">
                                                                <i class="fa fa-book"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <div class="course-info">
                                                <div class="course-author">
                                                <!-- {{-- <img src="{{asset('assets/views/assets/img/news/author2.jpg')}}" alt="course"> --}} -->
                                                    <div class="author-info top_margin">
                                                        <h4 class="title"><a href="#">University of the Punjab</a></h4>
                                                        <h5 class="subtitle">Prof. Dr. Muhammad Zakria Zakar</h5>
                                                    </div>
                                                </div>
                                                <h3 class="title"><a href="course-single.html"> PU is a Public  Research University </a></h3>

                                            </div>
                                        </div>
                                    </div>
                                </div><!--//ITEM-->

                                <div class="lgx-grid-item col-xs-12 col-sm-6 col-md-3 html">
                                    <div class="lgx-single-course">
                                        <div class="lgx-single-course-inner">
                                            <figure>
                                                <img src="{{asset('assets/views/assets/img/univerities/nust.jpg')}}" alt="course">
                                                <figcaption>
                                                    <div class="lgx-hover-link">
                                                        <div class="lgx-vertical">
                                                            <a href="course-single.html">
                                                                <i class="fa fa-book"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <div class="course-info">
                                                <div class="course-author">
                                                <!-- {{-- <img src="{{asset('assets/views/assets/img/news/author2.jpg')}}" alt="course"> --}} -->
                                                    <div class="author-info top_margin">
                                                        <h4 class="title"><a href="#">National University of Sciences and Technology</a></h4>
                                                        <h5 class="subtitle">Lieutenant General Naweed </h5>
                                                    </div>
                                                </div>
                                                <h3 class="title"><a href="course-single.html"> Defining Futures</a></h3>

                                            </div>
                                        </div>
                                    </div>
                                </div><!--//ITEM-->

                                <div class="lgx-grid-item col-xs-12 col-sm-6 col-md-3 joomla design">
                                    <div class="lgx-single-course">
                                        <div class="lgx-single-course-inner">
                                            <figure>
                                                <img src="{{asset('assets/views/assets/img/univerities/comset.jpg')}}" alt="course">
                                                <figcaption>
                                                    <div class="lgx-hover-link">
                                                        <div class="lgx-vertical">
                                                            <a href="course-single.html">
                                                                <i class="fa fa-book"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <div class="course-info">
                                                <div class="course-author">
                                                    {{-- <img src="{{asset('assets/views/assets/img/news/author5.jpg')}}" alt="course"> --}}
                                                    <div class="author-info top_margin">
                                                        <h4 class="title"><a href="#">COMSATS Institute of Information Technology</a></h4>
                                                        <h5 class="subtitle">Dr. Raheel Qamar</h5>
                                                    </div>
                                                </div>
                                                <h3 class="title"><a href="course-single.html"> Institute of IT</a></h3>

                                            </div>
                                        </div>
                                    </div>
                                </div><!--//ITEM-->

                                <div class="lgx-grid-item col-xs-12 col-sm-6 col-md-3 wordpress">
                                    <div class="lgx-single-course">
                                        <div class="lgx-single-course-inner">
                                            <figure>
                                                <img src="{{asset('assets/views/assets/img/univerities/gc.jpg')}}" alt="course">
                                                <figcaption>
                                                    <div class="lgx-hover-link">
                                                        <div class="lgx-vertical">
                                                            <a href="course-single.html">
                                                                <i class="fa fa-book"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <div class="course-info">
                                                <div class="course-author">
                                                    {{-- <img src="{{asset('assets/views/assets/img/news/author3.jpg')}}" alt="course"> --}}
                                                    <div class="author-info top_margin">
                                                        <h4 class="title"><a href="#">Government College</a></h4>
                                                        <h5 class="subtitle">Muhammad Ali</h5>
                                                    </div>
                                                </div>
                                                <h3 class="title"><a href="course-single.html"> Education People for Tomorrow</a></h3>

                                            </div>
                                        </div>
                                    </div>
                                </div><!--//ITEM-->



                            </div>
                        </div>
                        <!--//. ROW-->

                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="lgx-getintouch">

                                    <a class="lgx-btn lgx-btn-contact rippler rippler-default" href="{{route('velox-course')}}">View all Course</a>
                                </h3>
                            </div>
                        </div>

                    </div>




                    </div>




                            </div>
                        </div>



                    </div>

                </div>
            </div>
            <!--//. ROW-->



        </div>

    </div>
    </div>
    </div>
</section>
