<section>
    <div class="lgx-slider"> <!--lgx-slider-content -->
        <div class="lgx-banner-style">
            <div class="lgx-inner">

                <div id="lgx-main-slider" class="owl-carousel">

                    <!--SLIDER ITEM 1-->

                    <div class="lgx-item-common">

                        <div class="col-sm-12g">
                            <div class="slider-text-single">
                                <figure>
                                    <img src="{{asset('assets/views/assets/img/banaer8.jpg')}}" alt="cv"/>
                                    <figcaption>
                                        <div class="lgx-container">
                                            <div class="lgx-hover-link">
                                                <div class="lgx-vertical">
                                                    <div class="lgx-banner-info">  <!--lgx-banner-info-white-->
                                                        <h3 class="subtitle lgx-fadeInLeft-one">Premeum Quality</h3>
                                                        <h2 class="title lgx-fadeInLeft-two">Education plus for Futures</h2>
                                                        <p class="text lgx-fadeInLeft-three">Aentegers sollicitudin molestie ante et dictum laoreet we are the Excilent. <br/> Nascetur ridiculus mus. Proin porta lectus eleifend. </p>
                                                        <div class="btn-area lgx-fadeInLeft-four">
                                                            <a class="lgx-btn" href="#">All Cources</a>
                                                            <a class="lgx-btn lgx-btn-border" href="#">All Cources</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div> <!--//.col-->
                    </div>
                    <!--SLIDER ITEM 1 End-->

                    <!--SLIDER ITEM 2-->
                    <div class="lgx-item-common">
                        <div class="col-sm-12g">
                            <div class="slider-text-single">
                                <figure>
                                    <img src="{{asset('assets/views/assets/img/bannersp3.jpg')}}" alt="cv"/>
                                    <figcaption>
                                        <div class="lgx-container">
                                            <div class="lgx-hover-link">
                                                <div class="lgx-vertical">
                                                    <div class="lgx-banner-info lgx-banner-info-center"> <!--lgx-banner-info-white-->
                                                        <h3 class="subtitle lgx-zoomIn-one">Premeum Quality</h3>
                                                        <h2 class="title lgx-zoomIn-two">Education plus for Futures</h2>
                                                        <p class="text lgx-zoomIn-three">Aentegers sollicitudin molestie ante et dictum laoreet we are the Excilent. <br/> Nascetur ridiculus mus. Proin porta lectus eleifend. </p>
                                                        <div class="btn-area lgx-zoomIn-four">
                                                            <a class="lgx-btn" href="#">All Cources</a>
                                                            <a class="lgx-btn lgx-btn-border" href="#">All Cources</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div> <!--//.col-->
                    </div>
                    <!--SLIDER ITEM 2 End-->

                    <!--SLIDER ITEM 3-->
                    <div class="lgx-item-common">
                        <div class="col-sm-12g">
                            <div class="slider-text-single">
                                <figure>
                                    <img src="{{asset('assets/views/assets/img/bannersp2.jpg')}}" alt="cv"/>
                                    <figcaption>
                                        <div class="lgx-container">
                                            <div class="lgx-hover-link">
                                                <div class="lgx-vertical">
                                                    <div class="lgx-banner-info lgx-banner-info-right"> <!--lgx-banner-info-white-->
                                                        <h3 class="subtitle lgx-zoomIn-one">Premeum Quality</h3>
                                                        <h2 class="title lgx-zoomIn-two">Education <span>Template</span></h2>
                                                        <p class="text lgx-zoomIn-three">Aentegers sollicitudin molestie ante et dictum laoreet we are the Excilent. <br/> Nascetur ridiculus mus. Proin porta lectus eleifend. </p>
                                                        <div class="btn-area lgx-zoomIn-four">
                                                            <a class="lgx-btn" href="#">All Cources</a>
                                                            <a class="lgx-btn lgx-btn-border" href="#">All Cources</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div> <!--//.col-->
                    </div>
                    <!--SLIDER ITEM 3 End-->

                </div> <!--//.lgx-main-slider-->


                <!-- //.CONTAINER -->
            </div>
            <!-- //.INNER -->
        </div>
    </div>
</section>
