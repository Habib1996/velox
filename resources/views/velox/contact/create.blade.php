{!! Form::open(['route'=> 'velox-contact-store']) !!}
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('Name', 'Name') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Habib Siddiqui']) !!}
        </div>
        @if ($errors->has('name'))
            <span class="text-danger"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
    </div>
</div>



<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('email', 'Email') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'VELOX']) !!}
        </div>
        @if ($errors->has('email'))
            <span class="text-danger"><strong>{{ $errors->first('email') }}</strong></span>
        @endif
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('phone', 'Phone') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('phone',null, ['class' => 'form-control' , 'placeholder'=> '+92 303 6083548']) !!}
        </div>
        @if ($errors->has('phone'))
            <span class="text-danger"><strong>{{ $errors->first('phone') }}</strong></span>
        @endif
    </div>
</div>


<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('message', 'Message') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::textarea('message',null, ['class' => 'form-control' , 'placeholder'=> 'This will provide you information about all Courses']) !!}
        </div>
        @if ($errors->has('message'))
            <span class="text-danger"><strong>{{ $errors->first('message') }}</strong></span>
        @endif
    </div>
</div>


{{--<input type="hidden" name="user_type_id" value="{{\App\Interfaces\IRecordType::USER_TYPE_ADMIN}}">--}}
<div class="form-group col-sm-12 text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="" class="btn btn-default">Cancel</a>
</div>
{!! Form::close() !!}