<section>
    <div class="lgx-banner">
        <div class="lgx-banner-style">
            <div class="lgx-inner lgx-inner-fixed">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-banner-info  lgx-banner-info-white">
                                <h3 class="subtitle">Contact us</h3>
                                <h2 class="title">Education <span>Template</span></h2>
                                <p class="text">Aentegers sollicitudin molestie ante et dictum laoreet we are the Excilent. <br/> Nascetur ridiculus mus. Proin porta lectus eleifend. </p>
                                <div class="btn-area">
                                    <a class="lgx-btn" href="registration.html">Registration</a>
                                    <a class="lgx-btn lgx-btn-border" href="courses.html">All Cources</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//.ROW-->
                </div>
                <!-- //.CONTAINER -->
            </div>
            <!-- //.INNER -->
        </div>
    </div>
</section>