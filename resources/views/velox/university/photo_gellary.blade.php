<section>
    <div id="lgx-photo-gallery" class="lgx-photo-gallery"> <!--lgx-gallery-without-subsctibe-->
        <div id="lgx-memorisinner" class="lgx-memorisinner">
            <div class="lgx-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading">
                                <h2 class="heading-title">Photo Gallery</h2>
                                <h4 class="heading-subtitle">Some Amazing Think From Our Campus</h4>
                            </div>
                        </div>
                    </div>
                    <div class="lgx-gallery-area">
                        <div  class="lgx-gallery-single">
                            <figure>
                                <img title="Memories One" src="{{asset('assets/views/assets/img/gallery/gallery1.jpg')}}" alt="Memories one"/>
                                <figcaption class="lgx-figcaption">
                                    <div class="lgx-hover-link">
                                        <div class="lgx-vertical">
                                            <a title="Memories One" href="assets/img/gallery/gallery1.jpg">
                                                <i class="fa fa-search fa-2x"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                        <div  class="lgx-gallery-single">
                            <figure>
                                <img src="{{asset('assets/views/assets/img/gallery/gallery2.jpg')}}" alt="Memories Two" title="Memories Two" />
                                <figcaption class="lgx-figcaption">
                                    <div class="lgx-hover-link">
                                        <div class="lgx-vertical">
                                            <a title="Memories Two" href="assets/img/gallery/gallery2.jpg">
                                                <i class="fa fa-search fa-2x"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                        <div  class="lgx-gallery-single">
                            <figure>
                                <img src="{{asset('assets/views/assets/img/gallery/gallery3.jpg')}}" alt="Memories Three" title="Memories Three" />
                                <figcaption class="lgx-figcaption">
                                    <div class="lgx-hover-link">
                                        <div class="lgx-vertical">
                                            <a title="Memories Three" href="assets/img/gallery/gallery3.jpg">
                                                <i class="fa fa-search fa-2x"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                        <div  class="lgx-gallery-single">
                            <figure>
                                <img src="{{asset('assets/views/assets/img/gallery/gallery4.jpg')}}" alt="Memories Four" title="Memories Four" />
                                <figcaption class="lgx-figcaption">
                                    <div class="lgx-hover-link">
                                        <div class="lgx-vertical">
                                            <a title="Memories Four" href="assets/img/gallery/gallery4.jpg">
                                                <i class="fa fa-search fa-2x"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="lgx-gallery-single">
                            <figure>
                                <img src="{{asset('assets/views/assets/img/gallery/gallery5.jpg')}}" alt="Memories Five" title="Memories Five" />
                                <figcaption class="lgx-figcaption">
                                    <div class="lgx-hover-link">
                                        <div class="lgx-vertical">
                                            <a title="Memories Five" href="assets/img/gallery/gallery5.jpg">
                                                <i class="fa fa-search fa-2x"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="lgx-gallery-single">
                            <figure>
                                <img src="{{asset('assets/views/assets/img/gallery/gallery6.jpg')}}" alt="Memories Six" title="Memories Six" />
                                <figcaption class="lgx-figcaption">
                                    <div class="lgx-hover-link">
                                        <div class="lgx-vertical">
                                            <a title="Memories Six" href="assets/img/gallery/gallery6.jpg">
                                                <i class="fa fa-search fa-2x"></i>
                                            </a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                </div> <!--//.CONAINER-->
            </div>
        </div><!--//.lgx CONTACT INNER-->
    </div>
</section>
