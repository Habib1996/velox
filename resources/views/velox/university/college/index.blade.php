<!doctype html>
<html class="no-js" lang="en">

<head>
    @include('velox.layouts.header')
</head>
<body class="home">

<div class="lgx-container ">

    <!--HEADER-->
@include('velox.layouts.nav_bar')
<!--HEADER END-->

    <!--HEADER-->
@include('velox.university.college.banner')
<!--HEADER END-->

    <!--ABOUT-->
@include('velox.university.college.about')
<!--ABOUT END-->

    <!--UNIVERSITY-->
@include('velox.university.college.college')
<!--UNIVERSITY END-->

    <!--FOOTER-->
@include('velox.layouts.footer')
<!--FOOTER END-->

</div>

@include('velox.layouts.footerfiles')


</body>

<!-- Mirrored from themearth.com/demo/html/educationplus/view/home-slider.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Feb 2018 12:37:12 GMT -->
</html>
