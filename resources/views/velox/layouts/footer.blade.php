<footer>
    <div id="lgx-footer" class="lgx-footer">
        <div class="lgx-getintouch-area">
            <div class="lgx-getintouch-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="lgx-getintouch">
                                {!! Form::open(['route'=> 'velox-subscribe']) !!}
                                <div class="form-group col-md-8 ">
                                    <input type="email" name="subscribe_email" class="form-control" id="lgxemail" placeholder="Enter email" required>
                                </div>

                                <button class="lgx-btn lgx-btn-contact rippler rippler-default" href="contact.html">Get In Touch</button>
                                {!! Form::close() !!}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lgx-footer-middle lgx-footer-middle-white"> <!--lgx-footer-middle-white-->
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="lgx-footer-single">
                            <a class="logo" href="index.html"><img src="{{asset('assets/views/assets/img/logo.png')}}" alt="Logo"></a>
                            <address>
                                51 Francis Street, Darlinghurst <br>
                                NSW 2010, United States
                            </address>
                            <ul class="list-unstyled lgx-address-info">
                                <li><i class="fa fa-phone"></i>+61 1900 654 368</li>
                                <li><i class="fa fa-envelope"></i>office@educationplus.com</li>
                                <li><i class="fa fa-envelope"></i>office2@educationplus.com.au</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="lgx-footer-single">
                            <h2 class="title">Universities</h2>
                            <ul class="list-unstyled">
                                <li><a href="#">University Of the Punjab</a></li>
                                <li><a href="#">University of Gujrat</a></li>
                                <li><a href="#">LUMS</a></li>
                                <li><a href="#">University of Lahore</a></li>
                                <li><a href="#">UET</a></li>
                                <li><a href="#">Fast University</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="lgx-footer-single">
                            <h2 class="title">Cources</h2>
                            <ul class="list-unstyled">
                                <li><a href="#">Interactive Python</a></li>
                                <li><a href="#">Advance Oracle</a></li>
                                <li><a href="#">Basic PHP</a></li>
                                <li><a href="#">Electronics</a></li>
                                <li><a href="#">Java Resources</a></li>
                                <li><a href="#">Courses Education</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="lgx-footer-single">
                            <h2 class="title">Instagram Feed</h2>
                            <div id="instafeed">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lgx-footer-bottom">
                    <div class="lgx-copyright">
                        <ul class="list-inline">
                            <li><a href="#">About</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                        <p>© 2017 EducationPlus is powered by <a href="http://www.themearth.com/">themearth.</a> Brands are the property of their respective owners.</p>
                    </div>
                    <ul class=" list-inline lgx-social-footer">
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                </div>

            </div>
            <!-- //.CONTAINER -->
        </div>
        <!-- //.footer Middle -->
    </div>
</footer>
