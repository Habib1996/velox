
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!-- The above 3 meta tags *must* come first in the head -->

    <!-- SITE TITLE -->
    <title>Education Plus</title>
    <meta name="description" content="Responsive Education HTML5 Template"/>
    <meta name="keywords" content="Bootstrap3,education,academic, university, Template, school , Responsive, HTML5"/>
    <meta name="author" content="themearth.com"/>

    <!-- twitter card starts from here, if you don't need remove this section -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@yourtwitterusername"/>
    <meta name="twitter:creator" content="@yourtwitterusername"/>
    <meta name="twitter:url" content="http://yourdomain.com/"/>
    <meta name="twitter:title" content="Your home page title, max 140 char"/>
    <!-- maximum 140 char -->
    <meta name="twitter:description" content="Your site description, maximum 140 char "/>
    <!-- maximum 140 char -->
    <meta name="twitter:image" content="assets/img/twittercardimg/twittercard-280-150.jpg"/>
    <!-- when you post this page url in twitter , this image will be shown -->
    <!-- twitter card ends from here -->

    <!-- facebook open graph starts from here, if you don't need then delete open graph related  -->
    <meta property="og:title" content="Your home page title"/>
    <meta property="og:url" content="http://your domain here.com"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:site_name" content="Your site name here"/>
    <!--meta property="fb:admins" content="" /-->  <!-- use this if you have  -->
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="assets/img/opengraph/fbphoto.jpg"/>
    <!-- when you post this page url in facebook , this image will be shown -->
    <!-- facebook open graph ends from here -->

    <!--  FAVICON AND TOUCH ICONS -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/assets/img/favicon.jpg')}}"/>
    <!-- this icon shows in browser toolbar -->
    <link rel="icon" type="image/x-icon" href="{{asset('assets/assets/img/favicon.jpg')}}"/>
    <!-- this icon shows in browser toolbar -->
    {{--<link rel="apple-touch-icon" sizes="57x57" href="{{asset('assets/assets/img/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('assets/assets/img/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/assets/img/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/assets/img/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/assets/img/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('assets/assets/img/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('assets/assets/img/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets/assets/img/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/assets/img/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="assets/image/png" sizes="192x192" href="{{asset('assets/assets/img/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="assets/image/png" sizes="32x32" href="{{asset('assets/assets/img/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="assets/image/png" sizes="96x96" href="{{asset('assets/assets/img/favicon/favicon-96x96.png')}}">--}}
    <link rel="icon" type="assets/image/png" href="{{asset('assets/assets/img/favicon/favicon')}}">
    <link rel="manifest" href="{{asset('assets/assets/img/favicon/manifest.json')}}">

    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="{{asset('assets/assets/libs/bootstrap/css/bootstrap.min.css')}}" media="all"/>

    <!-- FONT AWESOME -->
  
    <link rel="stylesheet" href="{{asset('assets/assets/libs/fontawesome/css/font-awesome.min.css')}}" media="all"/>

    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="{{asset('assets/views/assets/libs/maginificpopup/magnific-popup.css')}}" media="all"/>

    <!-- OWL CAROUSEL CSS -->
    <link rel="stylesheet" href="{{asset('assets/views/assets/libs/owlcarousel/owl.carousel.min.css')}}" media="all" />
    <link rel="stylesheet" href="{{asset('assets/views/assets/libs/owlcarousel/owl.theme.default.min.css')}}" media="all" />

    <!-- GOOGLE FONT -->
    <!--<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Merriweather:300,400,400i,700,900%7cLato:400,700,900"/>-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,900%7cUbuntu:300,300i,400,500,700"/>

    <link rel="stylesheet" href="{{asset('assets/views/assets/libs/animate/animate.css')}}" media="all" />
   <!-- Custom -->
   <link rel="stylesheet" href="{{asset('assets/views/assets/css/style-default.min.css')}}">
    <!-- MASTER  STYLESHEET  -->
    <link id="lgx-master-style" rel="stylesheet" href="{{asset('assets/views/assets/css/style-default.min.css')}}" media="all"/>

    <!-- MODERNIZER CSS  -->
    <script src="{{asset('assets/assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>
   <script src="{{asset('assets/assets/js/vendor/scroll.js')}}"></script>
