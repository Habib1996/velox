<header>
    <div id="lgx-header" class="lgx-header"> <!--lgx-header-container lgx-header-container-white lgx-header-container-brand lgx-header-singlemenu-->

        <div class="lgx-header-bottom lgx-header-bottom-fixed-black lgx-header-bottom-fixed-transparent lgx-header-bottom-scrol-black"> <!--lgx-header-bottom-black lgx-header-bottom-brand--><!--lgx-header-bottom-fixed lgx-header-bottom-fixed-black lgx-header-bottom-fixed-brand lgx-header-bottom-fixed-transparent--><!--lgx-header-bottom-scrol-black lgx-header-bottom-scrol-brand-->
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <nav class="navbar navbar-default lgx-navbar">
                            <div class="container">
                                <nav class="navbar navbar-default lgx-navbar lgx-navbar-search">
                                    <div class="lgxcontainer">
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                                    data-target=".navbar-collapse">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <a href="#toggle-search" class="hidden-lg hidden-md hidden-sm lgx-search-mobile search-animate"><span class="glyphicon glyphicon-search"></span></a>
                                            <div class="lgx-logo">
                                                <a href="index.html" class="lgx-scroll">
                                                    <img  src="{{asset('assets/views/assets/img/logo2.png')}}" alt="Logo"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="collapse navbar-collapse">
                                            <ul class="nav navbar-nav lgx-nav">
                                                <li>
                                                  <a href="{{route('velox-home')}}">Home</a>

                                                <li>
                                                <li>
                                                    <a href="" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">University <span class="caret"></span></a>
                                                    <ul class="dropdown-menu multi-level">
                                                         <li><a href="{{route('velox-private-university')}}">Private University</a></li>
                                                        <li><a href="{{route('velox-govt-university')}}">Government University</a></li>
                                                        <li><a href="{{route('velox-college-university')}}">Affiled Campuses</a></li>
                                                     
                                                    </ul>
                                                </li>
                                                <li><a class="lgx-scroll" href="{{route('velox-course')}}">Courses</a></li>

                                                <li>
                                                    <a href="index.html" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Events <span class="caret"></span></a>
                                                    <ul class="dropdown-menu multi-level">
                                                        <li><a href="">Local</a></li>
                                                        <li><a href="">International</a></li>

                                                    </ul>
                                                </li>
                                                <li><a class="lgx-scroll" href="{{route('velox-scholarship')}}">scholarships</a></li>
                                                <li><a class="lgx-scroll" href="{{route('velox-news')}}">News</a></li>
                                                            <li><a class="lgx-scroll active" href="{{route('velox-about')}}">About</a></li>
                                                <li><a class="lgx-scroll" href="{{route('velox-contact')}}">Contact</a></li>
                                                <li class="hidden-xs"><a href="#toggle-search" class="search-animate"><span class="glyphicon glyphicon-search"></span></a></li>
                                            </ul>
                                        </div>
                                        <!--/.nav-collapse -->
                                    </div>
                                    <div class="lgx-menu-search search-animate">
                                        <div class="container">
                                            <form action="#" method="GET" role="search">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="q" placeholder="Search for snippets and hit enter">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-danger" type="reset"><span class="glyphicon glyphicon-remove"></span></button>
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                            <!-- /.container -->
                        </nav>
                    </div>
                </div>
                <!--//.ROW-->
            </div>
            <!-- //.CONTAINER -->
        </div>
        <!-- //.INNER-->
    </div>
</header>
