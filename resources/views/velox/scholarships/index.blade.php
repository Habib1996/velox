<!doctype html>
<html class="no-js" lang="en">

<head>
    @include('velox.layouts.header')
</head>
<body class="home">

<div class="lgx-container ">

<!--HEADER-->
@include('velox.layouts.nav_bar')
<!--HEADER END-->

<!--HEADER-->
@include('velox.scholarships.banaer')
<!--HEADER END-->


{{-- CONTAT FROM --}}

@include('velox.scholarships.scholarship_content')
{{-- END CONTACT FROM --}}

<!--FOOTER-->
 @include('velox.layouts.footer')
<!--FOOTER END-->

</div>

 @include('velox.layouts.footerfiles')

</body>

<!-- Mirrored from themearth.com/demo/html/educationplus/view/home-slider.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Feb 2018 12:37:12 GMT -->
</html>
