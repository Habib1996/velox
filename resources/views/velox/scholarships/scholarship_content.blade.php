<!--EVENTS-->
<section>
    <div id="lgx-events" class="lgx-events lgx-event-normal">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-events-area">
                            <div class="row">
                                <div class="lgx-single-event">
                                    <div class="thumb">
                                        <a href="event-single.html"><img src="{{ asset('assets/views/assets/img/events/event2.jpg') }}" alt="event"></a>
                                    </div>
                                    <div class="event-info">
                                        <a class="date" href="#">June 14, 2017</a>
                                        <h4 class="location"> Swick Hill Street, Charlotte, UK</h4>
                                        <h3 class="title"><a href="event-single.html">I no longer understand my PhD dissertation on Education my PhD </a></h3>
                                    </div>
                                </div>
                                
                                
                                <div class="lgx-single-event">
                                    <div class="thumb">
                                        <a href="event-single.html"><img src="{{ asset('assets/views/assets/img/events/event2.jpg') }}" alt="event"></a>
                                    </div>
                                    <div class="event-info">
                                        <a class="date" href="#">June 14, 2017</a>
                                        <h4 class="location"> Swick Hill Street, Charlotte, UK</h4>
                                        <h3 class="title"><a href="event-single.html">I no longer understand my PhD dissertation on Education</a></h3>
                                    </div>
                                </div>
                                <div class="lgx-single-event">
                                    <div class="thumb">
                                        <a href="event-single.html"><img src="{{ asset('assets/views/assets/img/events/event2.jpg') }}" alt="event"></a>
                                    </div>
                                    <div class="event-info">
                                        <a class="date" href="#">December 24, 2016</a>
                                        <h4 class="location"> Swick Hill Street, Charlotte, UK</h4>
                                        <h3 class="title"><a href="event-single.html">The digital revolution in higher education has already happened</a></h3>
                                    </div>
                                </div>
                                <div class="lgx-single-event">
                                    <div class="thumb">
                                        <a href="event-single.html"><img src="{{ asset('assets/views/assets/img/events/event2.jpg') }}" alt="event"></a>
                                    </div>
                                    <div class="event-info">
                                        <a class="date" href="#">March 30, 2015</a>
                                        <h4 class="location"> Swick Hill Street, Charlotte, UK</h4>
                                        <h3 class="title"><a href="event-single.html">UX Education: Designing free Online Learning Curriculum</a></h3>
                                    </div>
                                </div>
                                <div class="lgx-single-event">
                                    <div class="thumb">
                                        <a href="event-single.html"><img src="{{ asset('assets/views/assets/img/events/event2.jpg') }}" alt="event"></a>
                                    </div>
                                    <div class="event-info">
                                        <a class="date" href="#">June 14, 2017</a>
                                        <h4 class="location"> Swick Hill Street, Charlotte, UK</h4>
                                        <h3 class="title"><a href="event-single.html">I no longer understand my PhD dissertation on Education</a></h3>
                                    </div>
                                </div>
                                <div class="lgx-single-event">
                                    <div class="thumb">
                                        <a href="event-single.html"><img src="{{ asset('assets/views/assets/img/events/event2.jpg') }}" alt="event"></a>
                                    </div>
                                    <div class="event-info">
                                        <a class="date" href="#">December 24, 2016</a>
                                        <h4 class="location"> Swick Hill Street, Charlotte, UK</h4>
                                        <h3 class="title"><a href="event-single.html">The digital revolution in higher education has already happened</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--//.ROW-->
            </div>
            <!-- //.CONTAINER -->
        </div>
        <!-- //.INNER -->
    </div>
</section>