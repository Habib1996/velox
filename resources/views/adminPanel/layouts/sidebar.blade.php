<div class="sidebar" data-color="rose" data-background-color="black" data-image="{{asset('img/sidebar-1.jpg')}}">
    <div class="logo">
        <a href="{{url('/')}}" class="simple-text logo-mini"></a>
        <a href="{{url('/')}}" class="simple-text logo-normal">Dashboard</a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{asset('img/default-user.png')}}"/>
            </div>
            <div class="user-info">
                <a data-toggle="collapse" href="#collapseExample" class="username">
                    <span> HABIB U SIDDIUQI
                      <b class="caret"></b>
                    </span>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span class="sidebar-mini"> <i class="icon-user"></i> </span>
                                <span class="sidebar-normal"> My Profile </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span class="sidebar-mini"> <i class="icon-update-profile"></i> </span>
                                <span class="sidebar-normal"> Edit Profile </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span class="sidebar-mini"> <i class="material-icons">settings</i> </span>
                                <span class="sidebar-normal"> Settings </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">

            <li class="nav-item @if(Request::url() == url('/')) active @endif ">
                <a class="nav-link" href="{{route('admin')}}">
                    <i class="material-icons">dashboard</i>
                    <p> Dashboard</p>
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{route('user')}}">
                    <i class="material-icons">person</i>
                    <p> Users</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#University">
                    <i class="fas fa-university"></i>
                    <p> University
                        <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse" id="University">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('university-pak')}}">
                                <i class="fas fa-university"></i>
                                <p>Private Universities</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('university-int')}}">
                                <i class="fas fa-university"></i>
                                <p>International Universities</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('university-college')}}">
                                <i class="fas fa-university"></i>
                                <p>Affiliated Campuses</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#event">
                    <i class="fas fa-calendar"></i>
                    <p> Event
                        <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse" id="event">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('event-pak')}}">
                                <i class="fas fa-calendar"></i>
                                <p>Pakistani Events</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('event-int')}}">
                                <i class="fas fa-calendar"></i>
                                <p>International Events</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#Scholarship">
                    <i class="fas fa-school"></i>
                    <p> Scholarship
                        <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse" id="Scholarship">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('scholarships-pak')}}">
                                <i class="fas fa-school"></i>
                                <p>Pakistani Scholarship</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('scholarship-int')}}">
                                <i class="fas fa-school"></i>
                                <p>International Scholarship</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('course')}}">
                    {{--<i class="material-icons">dashboard</i>--}}
                    <i class="fa fa-graduation-cap"></i>
                    <p> Courses</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('slider')}}">
                    {{--<i class="material-icons">dashboard</i>--}}
                    <i class="fa fa-graduation-cap"></i>
                    <p> Slider</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('banner')}}">
                    {{--<i class="material-icons">dashboard</i>--}}
                    <i class="fa fa-graduation-cap"></i>
                    <p> Banner</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('service')}}">
                    {{--<i class="material-icons">dashboard</i>--}}
                    <i class="fa fa-graduation-cap"></i>
                    <p> Services </p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('about')}}">
                    {{--<i class="material-icons">dashboard</i>--}}
                    <i class="fa fa-graduation-cap"></i>
                    <p> About</p>
                </a>
            </li>

        </ul>
    </div>
</div>