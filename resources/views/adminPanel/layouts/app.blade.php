<!DOCTYPE html>
<html>

@include('adminPanel.layouts.head')
<script type="text/javascript">
    if (document.readyState === 'complete') {
        if (window.location != window.parent.location) {
            const elements = document.getElementsByClassName("iframe-extern");
            while (elemnts.lenght > 0) elements[0].remove();
            // $(".iframe-extern").remove();
        }
    };
</script>
<body>
<input type="hidden" id="baseurl" value="{{url('/')}}">
{{--@if (!Auth::guest())--}}
    <div class="wrapper">
        @include('adminPanel.layouts.sidebar')
        <div class="main-panel">
        @include('adminPanel.layouts.header')
        <div class="content">
            <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--@endif--}}
    <div class="sidebar-overlay" data-reff=""></div>

@include('adminPanel.layouts.footer')
</body>
</html>