<nav class="navbar navbar-expand-lg navbar-dark bg-dark  navbar-absolute fixed-top float-right">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="navbar-minimize">
                <a href="{{ url()->previous() }}" class="btn btn-success">Back</a>

            </div>
        </div>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="https://creative-tim.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i> HABIB U SIDDIQUI {{--{{\App\Helpers\CustomerHelper::getName()}}--}}
                        <p>
                            <span class="d-lg-none d-md-block">Some Actions<b class="caret"></b></span>
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        {!! Form::open(['route' => 'logout']) !!}
                            <a class="dropdown-item" href="#">Profile</a>
                            <a class="dropdown-item" href="#" onclick="this.parentNode.submit();">Logout</a>
                        {!! Form::close() !!}
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>