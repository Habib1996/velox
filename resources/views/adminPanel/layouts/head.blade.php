<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="userId" content="{{Auth::check() ? Auth::user()->id : null}}">
    <link rel="icon" href="{{asset('img/favicon.ico')}}">
    <title>

        {{config('APP_NAME','VELOX')}}

    </title>

    {{--<link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700" rel="stylesheet">--}}
    <link href="{{asset('fonts/material/icon.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/material-dashboard.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/demo.css')}}">
    <link rel="stylesheet" href="{{asset('fonts/icomoon/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('fonts/fontawesome/css/fontawesome-all.min.css')}}">
    {{--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons">--}}
    <link rel="stylesheet" href="{{asset('kartik/css/fileinput.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/site.css')}}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{asset('js/jquery.min.js')}}"></script>
</head>
