@extends('adminPanel.layouts.app')

@section('content')


    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-users"></i>
                            </div>
                            <h4 class="card-title">DASHBOARD</h4>
@include('adminPanel.home.services')


                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                {{--@include('adminPanel.slider.table')--}}

                            </div>
                        </div>
                        {{--<div class="pagination-wrapper"> {!! $sliders->appends(['search' => Request::get('search')])->render() !!} </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

