<table class="table table-striped table-bordered table-active ">
    <thead class="">
    <tr class="text-center">
        <th style="width: 100px">Image</th>
        <th>University Name</th>
        <th>University Moto</th>
        <th>VC Name</th>
        <th>World Ranking</th>
        <th>Country Ranking</th>
        <th>Status</th>
        <th style="width: 120px">Action</th>
    </tr>

    </thead>
    <tbody>

    @foreach($university as $universities)
        <tr class="">


            <td><img src="{{asset('uploads/universities/'.$universities->image)}}" alt="" width="100px"></td>
            <td>{{$universities->university_name}}</td>
            <td>{{$universities->university_moto}}</td>
            <td>{{$universities->vc_name}}</td>
            <td>{{$universities->world_ranking}}</td>
            <td>{{$universities->pakistan_ranking}}</td>
            <td>{!! \App\Helpers\StatusHelper::getStatus($universities->status) !!}</td>
            <td class="td-actions text-right">
                <a href="{{route('university-status', $universities->id)}}" title="status" rel="tooltip"
                   class="btn btn-success btn-round">
                    <i class="material-icons">edit</i>
                </a>
                @if($universities->university_type == 1)
                    <a href="{{route('university-pakistan-show',$universities->id)}}" title="view" rel="tooltip" class="btn btn-info btn-round">
                        <i class="material-icons">person</i>
                    </a>
                    <a href="{{route('university-pak-edit', $universities->id)}}" title="edit" rel="tooltip"
                       class="btn btn-success btn-round">
                        <i class="material-icons">edit</i>
                    </a>
                @elseif($universities->university_type == 2)
                    <a href="{{route('university-international-show',$universities->id)}}" title="view" rel="tooltip" class="btn btn-info btn-round">
                        <i class="material-icons">person</i>
                    </a>
                    <a href="{{route('university-int-edit', $universities->id)}}" title="edit" rel="tooltip"
                       class="btn btn-success btn-round">
                        <i class="material-icons">edit</i>
                    </a>
                    @else
                    <a href="{{route('university-college-show',$universities->id)}}" title="view" rel="tooltip" class="btn btn-info btn-round">
                        <i class="material-icons">person</i>
                    </a>
                    <a href="{{route('university-college-edit', $universities->id)}}" title="edit" rel="tooltip"
                       class="btn btn-success btn-round">
                        <i class="material-icons">edit</i>
                    </a>

                @endif

                <a href="{{route('university-destroy', $universities->id)}}" onclick="return confirm('Are you sure?');" title="delete" rel="tooltip" class="btn btn-danger btn-round">
                    <i class="material-icons">close</i>
                </a>
            </td>


        </tr>

    @endforeach

    </tbody>
</table>


