@extends('adminPanel.layouts.app')

@section('content')
    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-users"></i>
                            </div>
                            <h4 class="card-title">University Show</h4>

                        </div>
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-8">
                                    <table class="table">
                                        <tr>
                                            <th>ID</th>
                                            <td class="border-top">{{$university->id}}</td>
                                        </tr>
                                        <tr>
                                            <th>Image</th>
                                            <td class="border-top"><img src="{{asset('uploads/universities/'.$university->image)}}" alt="" width="100px"></td>
                                        </tr>
                                        <tr>
                                            <th>University Name</th>
                                            <td class="border-top">{{$university->univeristy_name}}</td>
                                        </tr>
                                        <tr>
                                            <th>VC Name</th>
                                            <td class="border-top">{{$university->vc_name}}</td>
                                        </tr>
                                        <tr>
                                            <th>University Moto</th>
                                            <td class="border-top">{{$university->university_moto}}</td>
                                        </tr>
                                        <tr>
                                            <th>World Ranking</th>
                                            <td class="border-top">{{$university->world_ranking}}</td>
                                        </tr>
                                        <tr>
                                            <th>University Type</th>
                                            {{--<td class="border-top">{{$university->university_type}}</td>--}}
                                            @if($university->university_type == 1)
                                                <td class="border-top">Pakistan</td>
                                                @elseif ($university->university_type == 2)
                                                <td class="border-top">International</td>
                                                @else
                                                <td class="border-top">College</td>

                                            @endif
                                        </tr>
                                        <tr>
                                            <th>Pakistan Ranking</th>
                                            <td class="border-top">{{$university->pakistan_ranking}}</td>
                                        </tr>
                                        <tr>
                                            <th>World Ranking</th>
                                            <td class="border-top">{{$university->world_ranking}}</td>
                                        </tr>


                                    </table>

                                </div>
                                <div class="col-md-4">


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-rose card-header-icon">
                        <div class="card-icon">
                            <i class="fa fa-users"></i>

                        </div>
                        <h4 class="card-title">Department</h4>
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
                            Add Departments
                        </button>

                        <!-- Modal -->

                        </div>

                    <div class="card-body">
                            <div class="table-responsive">
    <table class="table table-striped table-bordered table-active">
        <thead class="">
        <tr class="text-center">
            <th style="width: 100px">Image</th>
            <th>Name</th>
            <th>Status</th>

            <th style="width: 120px">Action</th>
        </tr>

        </thead>
        <tbody>

        @foreach($department as $departments)
            <tr>


                <td><img src="{{asset('uploads/department/'.$departments->image)}}" alt="" width="100px"></td>
                <td>{{$departments->name}}</td>
                <td>{{$departments->description}}</td>
                <td>{{$departments->parent}}</td>

                <td class="td-actions text-right">
                    <a href="{{route('department-show', $departments->id)}}" title="view" rel="tooltip" class="btn btn-info btn-round">
                        <i class="material-icons">person</i>
                    </a>



                        <a href="{{route('department-edit', $departments->id)}}" title="edit" rel="tooltip"
                           class="btn btn-success btn-round">
                            <i class="material-icons">edit</i>
                        </a>


                    <a href="{{route('department-destroy', $departments->id)}}" title="delete" rel="tooltip" class="btn btn-danger btn-round">
                        <i class="material-icons">close</i>
                    </a>
                </td>


            </tr>


        @endforeach
        </tbody>

    </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 70%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Departments</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('adminPanel.university.department_form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>


@endsection

