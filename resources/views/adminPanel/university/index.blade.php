@extends('adminPanel.layouts.app')

@section('content')


    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-users"></i>
                            </div>
                            @if(!empty($pakistan))
                                <h4 class="card-title">Pakistan Universities</h4>
                                <a href="{{route('university-pak-create')}}" class="btn btn-primary float-right"><i
                                            class="fa fa-user-plus"></i> Add New</a>
                                 @elseif(!empty($international))
                                <h4 class="card-title">International Universities</h4>
                                <a href="{{route('university-int-create')}}" class="btn btn-primary float-right"><i
                                            class="fa fa-user-plus"></i> Add New</a>
                                 @else
                                <h4 class="card-title">Affiliated Colleges</h4>
                                <a href="{{route('university-college-create')}}" class="btn btn-primary float-right"><i
                                            class="fa fa-user-plus"></i> Add New</a>
                            @endif


                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                @include('adminPanel.university.table')
                            </div>
                        </div>
                        <div class="pagination-wrapper"> {!! $university->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

