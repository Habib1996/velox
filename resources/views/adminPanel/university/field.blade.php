<!-- First Name Field -->
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('University Name', 'University Name:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('university_name', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('university_name'))
            <span class="text-danger"><strong>{{ $errors->first('university_name') }}</strong></span>
        @endif
    </div>
</div>

<div class="row" hidden>
    <label class="col-md-3 col-form-label">
        {!! Form::label('scholarship_type', 'scholarship Type:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::select('scholarship_type',['Pakistani','International'],null, ['class' => 'form-control selectdrop']) !!}
            @if(!empty($pakistan))
                <input type="hidden" name="university_type" value="1">
            @elseif(!empty($international))
                <input type="hidden" name="university_type" value="2">
                @else
                <input type="hidden" name="university_type" value="3">
            @endif
        </div>
        @if ($errors->has('speaker_name'))
            <span class="text-danger"><strong>{{ $errors->first('scholarshiptype') }}</strong></span>
        @endif
    </div>
</div>



    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('VC Name', 'VC Name:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::text('vc_name', null, ['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('vc_name'))
                <span class="text-danger"><strong>{{ $errors->first('vc_name') }}</strong></span>
            @endif
        </div>
    </div>

    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('University Moto', 'University Moto') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::text('university_moto',null, ['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('university_moto'))
                <span class="text-danger"><strong>{{ $errors->first('university_moto') }}</strong></span>
            @endif
        </div>
    </div>




    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('World Ranking', 'World Ranking:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::text('world_ranking',null, ['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('world_ranking'))
                <span class="text-danger"><strong>{{ $errors->first('world_ranking') }}</strong></span>
            @endif
        </div>
    </div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('Country Ranking', 'Country Ranking:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('pakistan_ranking',null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('pakistan_ranking'))
            <span class="text-danger"><strong>{{ $errors->first('pakistan_ranking') }}</strong></span>
        @endif
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        <label for="">Image</label>
    </label>
    <div class="col-md-9">
        <input name="image[]" type="file" class="file" multiple>

    </div>
</div>


{{--<input type="hidden" name="user_type_id" value="{{\App\Interfaces\IRecordType::USER_TYPE_ADMIN}}">--}}
<div class="form-group col-sm-12 text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="" class="btn btn-default">Cancel</a>
</div>
