
@extends('adminPanel.layouts.app')

@section('content')
    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-user-plus"></i>
                            </div>
                            @if(!empty($pakistan))
                                <h4 class="card-title">Pakistan University Edit</h4>
                            @elseif(!empty($international))
                                <h4 class="card-title">International University Edit</h4>
                                @else
                                  <h4 class="card-title">Affiliated College Edit</h4>
                            @endif
                        </div>
                        <div class="card-body">
                            {!! Form::model($university, ['route' => ['university-update', $university->id  ],'class' => 'form-horizontal','files' => true]) !!}
                               @include('adminPanel.university.field')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
