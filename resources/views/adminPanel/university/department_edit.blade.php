@extends('adminPanel.layouts.app')

@section('content')
    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-user-plus"></i>
                            </div>
                            <h4 class="card-title">Department Update</h4>

                        </div>
                        <div class="card-body">
                            {!! Form::model($department, ['route' => ['department-update', $department->id], 'class' => 'form-horizontal','files' => true]) !!}

                                <div class="row">
                                    <label class="col-md-3 col-form-label">
                                        {!! Form::label('Department Name', 'Department Name:') !!}
                                    </label>
                                    <div class="col-md-9">
                                        <div class="form-group has-default">
                                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                        </div>
                                        @if ($errors->has('department_name'))
                                            <span class="text-danger"><strong>{{ $errors->first('department_name') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row" hidden>
                                    {!! Form::hidden('parent',null,[]) !!}
                                    <input type="hidden" name="university_type" value="{{$university->university_type}}">
                                </div>




                                <div class="row">
                                    <label class="col-md-3 col-form-label">
                                        <label for="">Image</label>
                                    </label>
                                    <div class="col-md-9">
                                        <input name="image[]" type="file" class="file" multiple>

                                    </div>
                                </div>


                                <div class="row">
                                    <label class="col-md-3 col-form-label">
                                        {!! Form::label('Description', 'Description:') !!}
                                    </label>
                                    <div class="col-md-9">
                                        <div class="form-group has-default">
                                            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                                        </div>
                                        @if ($errors->has('description'))
                                            <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                {{--<input type="hidden" name="user_type_id" value="{{\App\Interfaces\IRecordType::USER_TYPE_ADMIN}}">--}}
                                <div class="form-group col-sm-12 text-right">
                                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                    <a href="" class="btn btn-default">Cancel</a>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
