{!! Form::open(['route' => 'department-store', 'files' => true]) !!}<!-- First Name Field -->
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('Department Name', 'Department Name:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('department_name', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('department_name'))
            <span class="text-danger"><strong>{{ $errors->first('department_name') }}</strong></span>
        @endif
    </div>
</div>

<div class="row" hidden>
    <input type="hidden" name="parent" value="{{$university->id}}">
    <input type="hidden" name="university_type" value="{{$university->university_type}}">

</div>




<div class="row">
    <label class="col-md-3 col-form-label">
        <label for="">Image</label>
    </label>
    <div class="col-md-9">
        <input name="image[]" type="file" class="file" multiple>

    </div>
</div>


<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('Description', 'Description:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('description'))
            <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
        @endif
    </div>
</div>

{{--<input type="hidden" name="user_type_id" value="{{\App\Interfaces\IRecordType::USER_TYPE_ADMIN}}">--}}
<div class="form-group col-sm-12 text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="" class="btn btn-default">Cancel</a>
</div>
{!! Form::close() !!}