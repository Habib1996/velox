<table class="table table-bordered table-striped table-active">
    <thead class="">
    <tr>

        <th style="width: 110px">Image</th>
        <th style="width: 130px">Slider Name</th>
        <th style="width: 130px">Heading</th>
        <th style="width: 130px">Button</th>
        {{--<th>Description</th>--}}
        <th style="width: 70px;">Action</th>
    </tr>

    </thead>
    <tbody>

@foreach($banner as $banners)
        <tr class="">
            <td><img src="{{asset('uploads/banners/'.$banners->image)}}" alt="" width="100px"></td>
            <td>{{$banners->banner_name}}</td>
            <td>{{$banners->heading}}</td>
            <td>{{$banners->button}}</td>

            <td class="td-actions text-right">
                <a href="{{route('banner-show', $banners->id)}}" title="view" rel="tooltip" class="btn btn-success btn-round">
                    <i class="material-icons">person</i>
                </a>

                <a href="{{route('banner-edit', $banners->id)}}" title="edit" rel="tooltip" class="btn btn-success btn-round">
                    <i class="material-icons">edit</i>
                </a>
                <a href="{{route('banner-destroy', $banners->id)}}" title="delete" rel="tooltip" class="btn btn-danger btn-round">
                    <i class="material-icons">close</i>
                </a>
            </td>


        </tr>
@endforeach

    </tbody>
</table>
