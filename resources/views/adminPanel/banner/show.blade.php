@extends('adminPanel.layouts.app')

@section('content')
    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-users"></i>
                            </div>
                            <h4 class="card-title">Banner Show</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <table class="table">
                                        <tr>
                                            <th>ID</th>
                                            <td class="border-top">{{$banner->id}}</td>
                                        </tr>
                                        <tr>
                                            <th>Image</th>
                                            <td class="border-top"><img src="{{asset('uploads/banners/'.$banner->image)}}" alt="" width="100px"></td>
                                        </tr>
                                        <tr>
                                            <th>Name</th>
                                            <td class="border-top">{{$banner->banner_name}}</td>
                                        </tr>
                                        <tr>
                                            <th>Heading</th>
                                            <td class="border-top">{{$banner->heading}}</td>
                                        </tr>
                                        <tr>
                                            <th>Button</th>
                                            <td class="border-top">{{$banner->button}}</td>
                                        </tr>
                                        <tr>
                                            <th>Description</th>
                                            <td class="border-top">{{$banner->description}}</td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
