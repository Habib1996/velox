@extends('adminPanel.layouts.app')

@section('content')
    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-user-plus"></i>
                            </div>
                            <h4 class="card-title">Banner Create</h4>

                        </div>
                        <div class="card-body">
                            {!! Form::open(['route' => 'banner-store', 'class' => 'form-horizontal','files' => true]) !!}
                            @include('adminPanel.banner.field')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
