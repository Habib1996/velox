<!-- First Name Field -->
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('Banner Name', 'Banner Name:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('banner_name', null, ['class' => 'form-control', 'placeholder' => 'Habib Siddiqui']) !!}
        </div>
        @if ($errors->has('banner_name'))
            <span class="text-danger"><strong>{{ $errors->first('banner_name') }}</strong></span>
        @endif
    </div>
</div>



<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('heading', 'Heading:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('heading', null, ['class' => 'form-control', 'placeholder' => 'VELOX']) !!}
        </div>
        @if ($errors->has('heading'))
            <span class="text-danger"><strong>{{ $errors->first('heading') }}</strong></span>
        @endif
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('button', 'Button:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('button', null, ['class' => 'form-control' , 'placeholder' => 'ALL Courses']) !!}
        </div>
        @if ($errors->has('button'))
            <span class="text-danger"><strong>{{ $errors->first('button') }}</strong></span>
        @endif
    </div>
</div>



<div class="row">
    <label class="col-md-3 col-form-label">
        <label for="">Image</label>
    </label>
    <div class="col-md-9">
        <input name="image[]" type="file" class="file" multiple>

    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('description', 'Description:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::textarea('description',null, ['class' => 'form-control' , 'placeholder'=> 'This will provide you information about all Courses']) !!}
        </div>
        @if ($errors->has('description'))
            <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
        @endif
    </div>
</div>


{{--<input type="hidden" name="user_type_id" value="{{\App\Interfaces\IRecordType::USER_TYPE_ADMIN}}">--}}
<div class="form-group col-sm-12 text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="" class="btn btn-default">Cancel</a>
</div>
