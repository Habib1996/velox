<table class="table table-bordered table-striped table-active">
    <thead class="">
    <tr>

        <th>Image</th>
        <th>Slider Name</th>
        <th>Heading</th>
        <th>Button</th>
        <th>Description</th>
        <th>Status</th>
        <th style="width: 120px;">Action</th>
    </tr>

    </thead>
    <tbody>

@foreach($sliders as $slider )
        <tr class="">

            <td><img src="{{asset('uploads/sliders/'.$slider->image)}}" alt="" width="100px"> </td>
            <td>{{$slider->slider_name}}</td>
            <td>{{$slider->heading}}</td>
            <td>{{$slider->button}}</td>
            <td>{{$slider->description}}</td>
            <td>{!! \App\Helpers\StatusHelper::getStatus($slider->status) !!}</td>


            <td class="td-actions text-right">
                <a href="{{route('slider-status', $slider->id)}}" title="status"  rel="tooltip" class="btn btn-default btn-round">
                    <i class="material-icons">person</i>
                </a>
                <a href="{{route('slider-show', $slider->id)}}" title="view"  rel="tooltip" class="btn btn-info btn-round">
                    <i class="material-icons">person</i>
                </a>
                <a href="{{route('slider-edit',$slider->id)}}" title="edit" rel="tooltip" class="btn btn-success btn-round">
                    <i class="material-icons">edit</i>
                </a>
                <a href="{{route('slider-destroy',$slider->id)}}" title="delete" rel="tooltip" class="btn btn-danger btn-round">
                    <i class="material-icons">close</i>
                </a>
            </td>


        </tr>

@endforeach
    </tbody>
</table>
