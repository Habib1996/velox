<table class="table table-bordered table-striped table-active">
    <thead class="">
    <tr>

        <th>Image</th>
        <th>Slider Name</th>
        <th>Heading</th>
        <th>Button</th>
        <th>Description</th>
        <th style="width: 120px;">Status</th>
    </tr>

    </thead>
    <tbody>

@foreach($sliders as $slider )
        <tr class="">
            {{--<td>{{$event->id}}</td>--}}
            {{--<td>{{$event->event_name}}</td>--}}
            {{--<td>{{$event->location}}</td>--}}
            {{--<td>{{$event->event_fee}}</td>--}}
            {{--<td>{{$event->status_id}}</td>--}}
            {{--<th>ID</th>--}}
            <td><img src="{{asset('uploads/sliders/'.$slider->image)}}" alt="" width="100px"> </td>
            <th>{{$slider->slider_name}}</th>
            <th>{{$slider->heading}}</th>
            <th>{{$slider->button}}</th>
            <th>{{$slider->description}}</th>


            <td class="td-actions text-right">
                <a href="{{route('slider-show', $slider->id)}}" title="view"  rel="tooltip" class="btn btn-info btn-round">
                    <i class="material-icons">person</i>
                </a>
                <a href="{{route('slider-edit',$slider->id)}}" title="edit" rel="tooltip" class="btn btn-success btn-round">
                    <i class="material-icons">edit</i>
                </a>
                <a href="{{route('slider-destroy',$slider->id)}}" title="delete" rel="tooltip" class="btn btn-danger btn-round">
                    <i class="material-icons">close</i>
                </a>
            </td>


        </tr>

@endforeach
    </tbody>
</table>
