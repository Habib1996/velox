<!-- First Name Field -->
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('scholarship Name', 'scholarship Name:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('scholarship_name', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('scholarship_name'))
            <span class="text-danger"><strong>{{ $errors->first('scholarship_name') }}</strong></span>
        @endif
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('start_date', 'Start Date:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::date('start_date', null, ['class' => 'form-control  ']) !!}
        </div>
        @if ($errors->has('start_date'))
            <span class="text-danger"><strong>{{ $errors->first('start_date') }}</strong></span>
        @endif
    </div>
</div>

@if(empty($user))
    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('last_date', 'Last Date:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::date('last_date', null, ['class' => 'form-control ']) !!}
            </div>
            @if ($errors->has('last_date'))
                <span class="text-danger"><strong>{{ $errors->first('last_date') }}</strong></span>
            @endif
        </div>
    </div>

    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('Eligibility Criteria', 'Eligibility Criteria:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::textarea('eligibility_criteria', null, ['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('eligibility_criteria'))
                <span class="text-danger"><strong>{{ $errors->first('eligibility_criteria') }}</strong></span>
            @endif
        </div>
    </div>

    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('Terms & Conditions', 'Terms & Conditions') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::textarea('terms_conditions',null, ['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('terms_conditions'))
                <span class="text-danger"><strong>{{ $errors->first('terms_conditions') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('location', 'Location:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::text('location', null, ['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('location'))
                <span class="text-danger"><strong>{{ $errors->first('location') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('organization', 'Organization:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::text('organization', null, ['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('organization'))
                <span class="text-danger"><strong>{{ $errors->first('organization') }}</strong></span>
            @endif
        </div>
    </div>

    <div class="row" hidden>
        <label class="col-md-3 col-form-label">
            {!! Form::label('scholarship_type', 'scholarship Type:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                                {!! Form::select('scholarship_type',['Pakistani','International'],null, ['class' => 'form-control selectdrop']) !!}
                @if(!empty($pakistan))
                    <input type="hidden" name="scholarship_type" value="1">
                @elseif(!empty($international))
                    <input type="hidden" name="scholarship_type" value="2">
                @endif
            </div>
            @if ($errors->has('speaker_name'))
                <span class="text-danger"><strong>{{ $errors->first('scholarshiptype') }}</strong></span>
            @endif
        </div>
    </div>

    <div class="row">
        <label class="col-md-3 col-form-label">
            <label for="">Image</label>
        </label>
        <div class="col-md-9">
            <input name="image[]" type="file" class="file" multiple>

        </div>
    </div>

    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('description', 'Description:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::textarea('description',null, ['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('speaker_name'))
                <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
            @endif
        </div>
    </div>

@endif
{{--<input type="hidden" name="user_type_id" value="{{\App\Interfaces\IRecordType::USER_TYPE_ADMIN}}">--}}
<div class="form-group col-sm-12 text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="" class="btn btn-default">Cancel</a>
</div>
