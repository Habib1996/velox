@extends('adminPanel.layouts.app')

@section('content')


    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-users"></i>
                            </div>
                             @if(!empty($pakistan))
                                <h4 class="card-title">Pakistan Scholarships</h4>
                                <a href="{{route('scholarship-pak-create')}}" class="btn btn-primary float-right"><i
                                            class="fa fa-user-plus"></i> Add New</a>

                              @else
                                <h4 class="card-title">International Scholarships</h4>
                                <a href="{{route('scholarship-int-create')}}" class="btn btn-primary float-right"><i
                                            class="fa fa-user-plus"></i> Add New</a>
                             @endif


                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                @include('adminPanel.scholarship.table')

                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pagination-wrapper float-right"> {!! $scholarships->appends(['search' => Request::get('search')])->render() !!} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

