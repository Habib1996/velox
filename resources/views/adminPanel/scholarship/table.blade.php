<table class="table table-bordered  table-striped">
    <thead class="">
    <tr>
        <th>ID</th>
        <th>Image</th>
        <th>Scholarship Name</th>
        <th>Location</th>
        <th>Eligibility</th>
        <th>Organization</th>
        <th>Status</th>
    </tr>

    </thead>
    <tbody>

@foreach($scholarships as $scholarship)
        <tr class="">
            <td>{{$scholarship->id}}</td>
            <th><img src="{{asset('uploads/scholarships/'.$scholarship->image)}}" alt="" width="100px"></th>
            <td>{{$scholarship->scholarship_name}}</td>
            <td>{{$scholarship->location}}</td>
            <td>{{$scholarship->eligibility_criteria}}</td>
            <td>{{$scholarship->organization}}</td>



            <td class="td-actions text-right">

                <a href="{{route('scholarship-show', $scholarship->id)}}" title="view"  rel="tooltip" class="btn btn-info btn-round">
                    <i class="material-icons">person</i>
                </a>
                @if($scholarship->scholarship_type == 1)
                    <a href="{{route('scholarship-pak-edit',$scholarship->id)}}" title="edit"  rel="tooltip" class="btn btn-success btn-round">
                        <i class="material-icons">edit</i>
                    </a>
                @else
                    <a href="{{route('scholarship-int-edit',$scholarship->id)}}" title="edit" rel="tooltip" class="btn btn-success btn-round">
                        <i class="material-icons">edit</i>
                    </a>
                @endif

                <a href="{{route('scholarship-destroy', $scholarship->id)}}" title="delete"  rel="tooltip" class="btn btn-danger btn-round">
                    <i class="material-icons">close</i>
                </a>
            </td>


        </tr>


@endforeach
    </tbody>
</table>
