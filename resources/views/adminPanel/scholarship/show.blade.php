@extends('adminPanel.layouts.app')

@section('content')
    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-users"></i>
                            </div>
                            @if($scholarship->scholarship_type == 1)
                            <h4 class="card-title">Pakistan Scholarship Show</h4>
                                @else
                                <h4 class="card-title">International Scholarship Show</h4>
                            @endif
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <table class="table">
                                        <tr>
                                            <th>ID</th>
                                            <td class="border-top">{{$scholarship->id}}</td>
                                        </tr>
                                        <tr>
                                            <th>Image</th>
                                            <td class="border-top"><img src="{{asset('uploads/scholarships/'.$scholarship->image)}}" alt="" width="100px"></td>
                                        </tr>
                                        <tr>
                                            <th>Scholarship Name</th>
                                            <td class="border-top">{{$scholarship->scholarship_name}}</td>
                                        </tr>
                                        <tr>
                                            <th>Scholarship Type</th>
                                            @if($scholarship->scholarship_type == 1)
                                            <td class="border-top">Pakistan</td>
                                                @else
                                                <td class="border-top">International</td>

                                            @endif
                                        </tr>
                                        <tr>
                                            <th>Start Date</th>
                                            <td class="border-top">{{$scholarship->start_date}}</td>
                                        </tr>
                                        <tr>
                                            <th>Last Date</th>
                                            <td class="border-top">{{$scholarship->last_date}}</td>
                                        </tr>
                                        <tr>
                                            <th>Eligibility Criteria</th>
                                            <td class="border-top">{{$scholarship->eligibility_criteria}}</td>
                                        </tr>
                                        <tr>
                                            <th>Terms & Conditions</th>
                                            <td class="border-top">{{$scholarship->terms_conditions}}</td>
                                        </tr>
                                        <tr>
                                            <th>Location</th>
                                            <td class="border-top">{{$scholarship->location}}</td>
                                        </tr>
                                        <tr>
                                            <th>Organization</th>
                                            <td class="border-top">{{$scholarship->organization}}</td>
                                        </tr>

                                        <tr>
                                            <th>Description</th>
                                            <td class="border-top">{{$scholarship->description}}</td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
