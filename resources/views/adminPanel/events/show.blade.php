@extends('adminPanel.layouts.app')

@section('content')
    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-users"></i>
                            </div>
                            @if($event->event_type == 1)
                                <h4 class="card-title">Pakistan Events Show</h4>
                            @else
                                <h4 class="card-title">International Events Show</h4>
                            @endif
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <table class="table">
                                        <tr>
                                            <th>ID</th>
                                            <td class="border-top">{{$event->id}}</td>
                                        </tr>
                                        <tr>
                                            <th>Image</th>
                                            <td class="border-top"><img src="{{asset('uploads/events/'.$event->image)}}" alt="" width="100px"></td>
                                        </tr>
                                        <tr>
                                            <th>Event Name</th>
                                            <td class="border-top">{{$event->event_name}}</td>
                                        </tr>
                                        <tr>
                                            <th>Event Type</th>
                                            @if ($event->event_type == 1 )
                                            <td class="border-top">Pakistan</td>
                                                @else
                                                <td class="border-top">International</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <th>Start Date</th>
                                            <td class="border-top">{{$event->start_date}}</td>
                                        </tr>
                                        <tr>
                                            <th>Last Date</th>
                                            <td class="border-top">{{$event->last_date}}</td>
                                        </tr>
                                        <tr>
                                            <th>Location</th>
                                            <td class="border-top">{{$event->location}}</td>
                                        </tr>
                                        <tr>
                                            <th>Event Fees</th>
                                            <td class="border-top">{{$event->event_fee}}</td>
                                        </tr>
                                        <tr>
                                            <th>Available Seats</th>
                                            <td class="border-top">{{$event->available_seats}}</td>
                                        </tr>
                                        <tr>
                                            <th>Sponsor Names</th>
                                            <td class="border-top">{{$event->sponsor_name}}</td>
                                        </tr>

                                        <tr>
                                            <th>Description</th>
                                            <td class="border-top">{{$event->description}}</td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
