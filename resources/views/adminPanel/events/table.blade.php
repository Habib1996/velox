<table class="table table-striped table-bordered table-active ">
    <thead class="">
    <tr class="text-center">
        <th style="width: 100px">Image</th>
        <th>Event Name</th>
        <th>Location</th>
        <th>Event Fee</th>
        <th style="width: 120px">Action</th>
    </tr>

    </thead>
    <tbody>

    @foreach($events as $event)
        <tr class="">
            <td><img src="{{asset('uploads/events/'.$event->image)}}" alt="" width="100px"> </td>
            <td>{{$event->event_name}}</td>
            <td>{{$event->location}}</td>
            <td>{{$event->event_fee}}</td>

            <td class="td-actions text-right">
                <a href="{{route('event-show', $event->id)}}" title="view"  rel="tooltip" class="btn btn-info btn-round">
                    <i class="material-icons">person</i>
                </a>
                @if($event->event_type == 1)
                <a href="{{route('event-pak-edit',$event->id)}}" title="edit"  rel="tooltip" class="btn btn-success btn-round">
                    <i class="material-icons">edit</i>
                </a>
                @else
                    <a href="{{route('event-int-edit',$event->id)}}" title="edit" rel="tooltip" class="btn btn-success btn-round">
                        <i class="material-icons">edit</i>
                    </a>
                @endif
                <a href="{{route('event-destroy',$event->id)}}" title="delete" rel="tooltip" class="btn btn-danger btn-round">
                    <i class="material-icons">close</i>
                </a>
            </td>


        </tr>
    @endforeach


    </tbody>
</table>
