<!-- First Name Field -->
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('Event Name', 'Event Name:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('event_name', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('event_name'))
            <span class="text-danger"><strong>{{ $errors->first('event_name') }}</strong></span>
        @endif
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('start_date', 'Start Date:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::date('start_date', null, ['class' => 'form-control  ']) !!}
        </div>
        @if ($errors->has('start_date'))
            <span class="text-danger"><strong>{{ $errors->first('start_date') }}</strong></span>
        @endif
    </div>
</div>

@if(empty($user))
    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('last_date', 'Last Date:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::date('last_date', null, ['class' => 'form-control ']) !!}
            </div>
            @if ($errors->has('last_date'))
                <span class="text-danger"><strong>{{ $errors->first('last_date') }}</strong></span>
            @endif
        </div>
    </div>

    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('location', 'Location:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::text('location', null, ['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('location'))
                <span class="text-danger"><strong>{{ $errors->first('location') }}</strong></span>
            @endif
        </div>
    </div>

    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('fees', 'Event Fee') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::text('event_fee',null, ['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('event_fee'))
                <span class="text-danger"><strong>{{ $errors->first('event_fee') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('available_seats ', 'Available Seats:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::text('available_seats', null,['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('available_seats'))
                <span class="text-danger"><strong>{{ $errors->first('available_seats') }}</strong></span>
            @endif
        </div>
    </div>

    <div class="row" hidden>
        <label class="col-md-3 col-form-label">
            {!! Form::label('event_type', 'Event Type:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
{{--                {!! Form::select('event_type',['Pakistani','International'],null, ['class' => 'form-control selectdrop']) !!}--}}
                @if(!empty($pakistan))
                <input type="hidden" name="event_type" value="1">
                @elseif(!empty($international))
                <input type="hidden" name="event_type" value="2">
                @endif
            </div>
            @if ($errors->has('speaker_name'))
                <span class="text-danger"><strong>{{ $errors->first('eventtype') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('sponsor_name', 'Sponsor Names:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::text('sponsor_name',null, ['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('sponsor_name'))
                <span class="text-danger"><strong>{{ $errors->first('sponsor_name') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="row">
        <label class="col-md-3 col-form-label">
            <label for="">Image</label>
        </label>
        <div class="col-md-9">
            <input name="image[]" type="file" class="file" multiple>

        </div>
    </div>

    <div class="row">
        <label class="col-md-3 col-form-label">
            {!! Form::label('description', 'Description:') !!}
        </label>
        <div class="col-md-9">
            <div class="form-group has-default">
                {!! Form::textarea('description',null, ['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('speaker_name'))
                <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
            @endif
        </div>
    </div>

@endif
{{--<input type="hidden" name="user_type_id" value="{{\App\Interfaces\IRecordType::USER_TYPE_ADMIN}}">--}}
<div class="form-group col-sm-12 text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="" class="btn btn-default">Cancel</a>
</div>
