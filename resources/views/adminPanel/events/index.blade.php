@extends('adminPanel.layouts.app')

@section('content')


    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-users"></i>
                            </div>
                            @if(!empty($pakistan))
                                <h4 class="card-title">Pakistan Events</h4>
                                <a href="{{route('event-pak-create')}}" class="btn btn-primary float-right"><i
                                            class="fa fa-user-plus"></i> Add New</a>
                            @else
                                <h4 class="card-title">International Events</h4>
                                <a href="{{route('event-int-create')}}" class="btn btn-primary float-right"><i
                                            class="fa fa-user-plus"></i> Add New</a>
                            @endif


                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                @include('adminPanel.events.table')

                            </div>
                        </div>
                        <div class="pagination-wrapper"> {!! $events->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

