<table class="table table-bordered table-striped table-active">
    <thead class="">
    <tr>

        <th>Image</th>
        <th>Slider Name</th>
        <th>Heading</th>
        <th>Button</th>
        <th>Description</th>
        <th style="width: 120px;">Status</th>
    </tr>

    </thead>
    <tbody>


        <tr class="">

            <td class="td-actions text-right">
                <a href="" title="view"  rel="tooltip" class="btn btn-info btn-round">
                    <i class="material-icons">person</i>
                </a>
                <a href="" title="edit" rel="tooltip" class="btn btn-success btn-round">
                    <i class="material-icons">edit</i>
                </a>
                <a href="" title="delete" rel="tooltip" class="btn btn-danger btn-round">
                    <i class="material-icons">close</i>
                </a>
            </td>


        </tr>


    </tbody>
</table>
