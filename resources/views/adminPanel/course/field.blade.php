<!-- First Name Field -->
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('Course Name', 'Course Name:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('course_name', null, ['class' => 'form-control', 'placeholder' => 'Habib Siddiqui']) !!}
        </div>
        @if ($errors->has('course_name'))
            <span class="text-danger"><strong>{{ $errors->first('course_name') }}</strong></span>
        @endif
    </div>
</div>



<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('start_date', 'Start Date:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('start_date', null, ['class' => 'form-control', 'placeholder' => 'VELOX']) !!}
        </div>
        @if ($errors->has('start_date'))
            <span class="text-danger"><strong>{{ $errors->first('start_date') }}</strong></span>
        @endif
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('last_date', 'Last Date:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('last_date', null, ['class' => 'form-control' , 'placeholder' => 'ALL Courses']) !!}
        </div>
        @if ($errors->has('last_date'))
            <span class="text-danger"><strong>{{ $errors->first('last_date') }}</strong></span>
        @endif
    </div>
</div>



<div class="row">
    <label class="col-md-3 col-form-label">
        <label for="">Image</label>
    </label>
    <div class="col-md-9">
        <input name="image[]" type="file" class="file" multiple>

    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('description', 'Description:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::textarea('description',null, ['class' => 'form-control' , 'placeholder'=> 'This will provide you information about all Courses']) !!}
        </div>
        @if ($errors->has('description'))
            <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
        @endif
    </div>
</div>


<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('learning_outcomes', 'Learning Outcomes:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('learning_outcomes',null, ['class' => 'form-control' , 'placeholder'=> 'This will provide you information about all Courses']) !!}
        </div>
        @if ($errors->has('learning_outcomes'))
            <span class="text-danger"><strong>{{ $errors->first('learning_outcomes') }}</strong></span>
        @endif
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('lectures', 'Lectures:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('description',null, ['class' => 'form-control' , 'placeholder'=> 'This will provide you information about all Courses']) !!}
        </div>
        @if ($errors->has('lectures'))
            <span class="text-danger"><strong>{{ $errors->first('lectures') }}</strong></span>
        @endif
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('features', 'Features:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('features',null, ['class' => 'form-control' , 'placeholder'=> 'This will provide you information about all Courses']) !!}
        </div>
        @if ($errors->has('features'))
            <span class="text-danger"><strong>{{ $errors->first('features') }}</strong></span>
        @endif
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('Instructor', 'Instructor:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('description',null, ['class' => 'form-control' , 'placeholder'=> 'This will provide you information about all Courses']) !!}
        </div>
        @if ($errors->has('Instructor'))
            <span class="text-danger"><strong>{{ $errors->first('Instructor') }}</strong></span>
        @endif
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('quizzes', 'Quizzes:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('quizzes',null, ['class' => 'form-control' , 'placeholder'=> 'This will provide you information about all Courses']) !!}
        </div>
        @if ($errors->has('quizzes'))
            <span class="text-danger"><strong>{{ $errors->first('quizzes') }}</strong></span>
        @endif
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('duration', 'Duration:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('duration',null, ['class' => 'form-control' , 'placeholder'=> 'This will provide you information about all Courses']) !!}
        </div>
        @if ($errors->has('duration'))
            <span class="text-danger"><strong>{{ $errors->first('duration') }}</strong></span>
        @endif
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('skill_level', 'Quizzes:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('skill_level',null, ['class' => 'form-control' , 'placeholder'=> 'This will provide you information about all Courses']) !!}
        </div>
        @if ($errors->has('skill_level'))
            <span class="text-danger"><strong>{{ $errors->first('skill_level') }}</strong></span>
        @endif
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('quizzes', 'Quizzes:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('description',null, ['class' => 'form-control' , 'placeholder'=> 'This will provide you information about all Courses']) !!}
        </div>
        @if ($errors->has('quizzes'))
            <span class="text-danger"><strong>{{ $errors->first('quizzes') }}</strong></span>
        @endif
    </div>
</div>

{{--<input type="hidden" name="user_type_id" value="{{\App\Interfaces\IRecordType::USER_TYPE_ADMIN}}">--}}
<div class="form-group col-sm-12 text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="" class="btn btn-default">Cancel</a>
</div>
