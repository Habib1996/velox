@extends('adminPanel.layouts.app')

@section('content')
    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-user-plus"></i>
                            </div>
                            <h4 class="card-title">About Update</h4>

                        </div>
                        <div class="card-body">
                            {!! Form::model($about, ['route' => ['about-update', $about->id], 'class' => 'form-horizontal','files' => true]) !!}
                            @include('adminPanel.about.field')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
