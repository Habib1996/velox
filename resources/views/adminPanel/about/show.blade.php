@extends('adminPanel.layouts.app')

@section('content')
    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-users"></i>

                            </div>
                            <h4 class="card-title">About Show</h4>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">

                                    <table class="table">
                                        <tr>
                                            <th>ID</th>
                                            <td class="border-top">{{$about->id}}</td>
                                        </tr>
                                        <tr>
                                            <th>Image</th>
                                            <td class="border-top"><img src="{{asset('uploads/abouts/'.$about->image)}}" alt="" width="100px"></td>
                                        </tr>
                                        <tr>
                                            <th>Heading</th>
                                            <td class="border-top">{{$about->heading}}</td>
                                        </tr>
                                        <tr>
                                            <th>SUB Heading</th>
                                            <td class="border-top">{{$about->sub_heading}}</td>
                                        </tr>
                                        <tr>
                                            <th>Description</th>
                                            <td class="border-top">{{$about->description}}</td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
