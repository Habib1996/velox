<table class="table table-bordered table-striped table-active">
    <thead class="">
    <tr>

        <th>Image</th>
        <th>Heading</th>
        <th>Sub Heading</th>
        <th>Description</th>
        <th>Status</th>
        <th style="width: 120px;">Action</th>
    </tr>

    </thead>
    <tbody>
    @foreach($about as $abouts)
        <tr class="">
            <th><img src="{{asset('uploads/abouts/'.$abouts->image)}}" alt="" width="100px"></th>
            <th>{{$abouts->heading}}</th>
            <th>{{$abouts->sub_heading}}</th>
            <th>{{$abouts->description}}</th>
            <td>{!! \App\Helpers\StatusHelper::getStatus($abouts->status) !!}</td>

            <td class="td-actions text-right">
                <a href="{{route('about-status', $abouts->id)}}" title="status"  rel="tooltip" class="btn btn-default btn-round">
                    <i class="material-icons">person</i>
                </a>

                <a href="{{route('about-show', $abouts->id)}}" title="view" rel="tooltip" class="btn btn-success btn-round">
                    <i class="material-icons">person</i>
                </a>
                {{--<button title="view" type="button" rel="tooltip" class="btn btn-info btn-round">
                    <i class="material-icons">person</i>
                </button>--}}
                <a href="{{route('about-edit', $abouts->id)}}" title="edit" rel="tooltip" class="btn btn-success btn-round">
                    <i class="material-icons">edit</i>
                </a>
                <a href="{{route('about-destroy', $abouts->id)}}" title="delete" rel="tooltip" class="btn btn-danger btn-round">
                    <i class="material-icons">close</i>
                </a>
            </td>

    @endforeach

    </tbody>
</table>
