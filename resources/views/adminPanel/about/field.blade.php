<!-- First Name Field -->
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('About Heading', 'About Heading:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('heading', null, ['class' => 'form-control', 'placeholder' => 'Habib Siddiqui']) !!}
        </div>
        @if ($errors->has('heading'))
            <span class="text-danger"><strong>{{ $errors->first('heading') }}</strong></span>
        @endif
    </div>
</div>


<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('sub heading', 'Sub Heading:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('sub_heading', null, ['class' => 'form-control' , 'placeholder' => 'ALL Courses']) !!}
        </div>
        @if ($errors->has('sub_heading'))
            <span class="text-danger"><strong>{{ $errors->first('sub_heading') }}</strong></span>
        @endif
    </div>
</div>



<div class="row">
    <label class="col-md-3 col-form-label">
        <label for="">Image</label>
    </label>
    <div class="col-md-9">
        <input name="image[]" type="file" class="file" multiple>

    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('type', 'Type:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::select('type',['Home','About', 'University'],null, ['class' => 'form-control select_drop' , 'placeholder'=> 'Select Type']) !!}
        </div>
        @if ($errors->has('type'))
            <span class="text-danger"><strong>{{ $errors->first('type') }}</strong></span>
        @endif
    </div>
</div>


<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('description', 'Description:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::textarea('description',null, ['class' => 'form-control' , 'placeholder'=> 'This will provide you information about all Courses']) !!}
        </div>
        @if ($errors->has('description'))
            <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
        @endif
    </div>
</div>


{{--<input type="hidden" name="user_type_id" value="{{\App\Interfaces\IRecordType::USER_TYPE_ADMIN}}">--}}
<div class="form-group col-sm-12 text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="" class="btn btn-default">Cancel</a>
</div>
