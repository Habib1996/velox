<table class="table table-striped table-bordered table-hover" id="users-table">
    <thead>
    <tr>
        <th>Id</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Contact Number</th>

        <th class="text-right">Action</th>
    </tr>
    </thead>
    <tbody>
{{--    @foreach($users as $user)--}}
        <tr>

            <th>Id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Contact Number</th>
            <td class="td-actions text-right">
                <a href="" title="view"  rel="tooltip" class="btn btn-info btn-round">
                    <i class="material-icons">person</i>
                </a>
                <a href="" title="edit" rel="tooltip" class="btn btn-success btn-round">
                    <i class="material-icons">edit</i>
                </a>
                <a href="" title="delete" rel="tooltip" class="btn btn-danger btn-round">
                    <i class="material-icons">close</i>
                </a>
            </td>
        </tr>
    {{--@endforeach--}}
    </tbody>
</table>