<!-- First Name Field -->
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('first_name', 'First Name:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('first_name'))
            <span class="text-danger"><strong>{{ $errors->first('first_name') }}</strong></span>
        @endif
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('last_name', 'Last Name:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

@if(empty($user))
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('username', 'Username:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::text('username', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('username'))
            <span class="text-danger"><strong>{{ $errors->first('username') }}</strong></span>
        @endif
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('email', 'Email:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('email'))
            <span class="text-danger"><strong>{{ $errors->first('email') }}</strong></span>
        @endif
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">
        {!! Form::label('password', 'Password:') !!}
    </label>
    <div class="col-md-9">
        <div class="form-group has-default">
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>
        @if ($errors->has('password'))
            <span class="text-danger"><strong>{{ $errors->first('password') }}</strong></span>
        @endif
    </div>
</div>
@endif
{{--<input type="hidden" name="user_type_id" value="{{\App\Interfaces\IRecordType::USER_TYPE_ADMIN}}">--}}
<div class="form-group col-sm-12 text-right">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    {{--<a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>--}}
</div>
