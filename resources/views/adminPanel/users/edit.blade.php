@extends('layouts.app')

@section('content')
    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="icon-update-profile fa-2x p-2"></i>
                            </div>
                            <h4 class="card-title">Update User</h4>
                        </div>
                        <div class="card-body">
                            {{--{!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch','class' => 'form-horizontal']) !!}--}}
                                {{--@include('users.fields')--}}
                            {{--{!! Form::close() !!}--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection