@extends('layouts.app')

@section('content')
    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-users"></i>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                            <div class="col-md-4 border">
                                <i class="icon-user" style="font-size: 20em"></i>
                            </div>
                            <div class="col-md-8">
                                <table class="table">
                                    <tr>
                                        <th>Full Name</th>
                                        <td class="border-top">{</td>
                                    </tr>
                                    <tr>
                                        <th>Mobile</th>
                                        <td>{{$user->mobile}}</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>{!! $user->email !!}</td>
                                    </tr>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
