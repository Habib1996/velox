<table class="table table-bordered table-striped table-active">
    <thead class="">
    <tr>

        <th>Image</th>
        <th>Slider Name</th>
        <th>Heading</th>
        <th>Button</th>
        <th>Description</th>
        <th style="width: 120px;">Status</th>
    </tr>

    </thead>
    <tbody>


    <tr class="">

{{--<!--        <td><img src="{{asset('uploads/sliders/'.$slider->image)}}" alt="" width="100px"> </td>-->--}}
{{--<!--        <th>{{$slider->slider_name}}</th>-->--}}
{{--<!--        <th>{{$slider->heading}}</th>-->--}}
{{--<!--        <th>{{$slider->button}}</th>-->--}}
{{--<!--        <th>{{$slider->description}}</th>-->--}}


        <td class="td-actions text-right">
            <a title="view"  rel="tooltip" class="btn btn-info btn-round">
                <i class="material-icons">person</i>
            </a>
            <a title="edit" rel="tooltip" class="btn btn-success btn-round">
                <i class="material-icons">edit</i>
            </a>
            <a  title="delete" rel="tooltip" class="btn btn-danger btn-round">
                <i class="material-icons">close</i>
            </a>
        </td>


    </tr>


    </tbody>
</table>
