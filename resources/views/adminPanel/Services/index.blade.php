@extends('adminPanel.layouts.app')

@section('content')


    <div class="clearfix"></div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-users"></i>
                            </div>
                            <h4 class="card-title">Services</h4>
                            <a href="{{route('slider-create')}}" class="btn btn-primary float-right"><i
                                        class="fa fa-user-plus"></i> Add New</a>


                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                @include('adminPanel.Services.table')

                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    {{--<div class="pagination-wrapper float-right"> {!! $sliders->appends(['search' => Request::get('search')])->render() !!} </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

