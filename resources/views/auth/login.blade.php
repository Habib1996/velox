<!DOCTYPE html>
<html>
@include('adminPanel.layouts.head')
<body class="off-canvas-sidebar login-page">
<div class="wrapper wrapper-full-page">
    <div class="page-header login-page header-filter" filter-color="black"
         style="background-image: url('{{asset('img/login.jpg')}}'); background-size: cover; background-position: top center;">
        <div class="container">
            <div class="col-md-4 col-sm-6 ml-auto mr-auto">
                {!! Form::open(['route' => 'login','class' => 'form','id' => 'LoginValidation']) !!}
                <div class="card card-login">
                    <div class="card-header card-header-rose text-center">
                        <h4 class="card-title">Login</h4>
                    </div>
                    <div class="card-body">
                        <div class="bmd-form-group mt-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">email</i>
                                    </span>
                                </div>
                                <label for="" class="bmd-label-floating ml-5 pl-2">Email</label>
                                <input type="email" class="form-control" name="email" value="{{old('email')}}" autofocus required>
                            </div>
                            @if ($errors->has('email'))
                                <span class="text-danger ml-5 pl-2"><strong>{{ $errors->first('email') }}</strong></span>
                            @endif
                        </div>
                        <div class="bmd-form-group mt-5">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">lock_outline</i>
                                    </span>
                                </div>
                                <label for="" class="bmd-label-floating ml-5 pl-2">Password</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            @if ($errors->has('password'))
                                <span class="text-danger ml-5 pl-2"><strong>{{ $errors->first('password') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <div class="card-footer justify-content-center">
                        <button class="btn btn-primary btn-lg">Login</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@include('adminPanel.layouts.footer')
</body>
</html>
