<?php


Route::group(['prefix' => 'velox'], function(){
    Route::get('/', 'velox\HomesController@index')->name('velox-home');
    Route::get('/about', 'velox\AboutsController@index')->name('velox-about');
    Route::get('/contact', 'velox\ContactsController@index')->name('velox-contact');
    Route::post('/contact/store', 'velox\ContactsController@store')->name('velox-contact-store');
    Route::get('/news', 'velox\NewsController@index')->name('velox-news');
    Route::get('/scholarship', 'velox\ScholarshipsController@index')->name('velox-scholarship');

    Route::group(['prefix' => 'course'], function(){
        Route::get('/', 'velox\CoursesController@index')->name('velox-course');
        Route::get('/show_course','velox\CoursesController@show_course')->name('velox-show-course');
    });

    Route::group(['prefix' => 'university'], function(){
        Route::get('/','velox\UniversitiesController@index')->name('velox-university');
        Route::get('/private','velox\UniversitiesController@private_index')->name('velox-private-university');
        Route::get('/govt','velox\UniversitiesController@govt_index')->name('velox-govt-university');
        Route::get('/colleges','velox\UniversitiesController@college_index')->name('velox-college-university');
        Route::get('/show/{id}', 'velox\UniversitiesController@private_show')->name('velox-private-university-show');
    });

    Route::post('/subscribe', 'velox\ContactsController@subscribe')->name('velox-subscribe');

});

Route::group(['prefix'=> 'admin','middleware' => 'auth'], function(){

    Route::get('/', 'adminPanel\HomesController@index')->name('admin');
//    Route::get('/passwordRest','')

    Route::group(['prefix' => 'university'], function(){

        Route::group(['prefix' => 'pakistan'],function(){
            Route::get('/', 'adminPanel\UniversitiesController@pak_index')->name('university-pak');
            Route::get('/create','adminPanel\UniversitiesController@pak_create')->name('university-pak-create');
            Route::get('/edit/{id}', 'adminPanel\UniversitiesController@pak_edit')->name('university-pak-edit');
            Route::get('/view/{id}','adminPanel\UniversitiesController@show')->name('university-pakistan-show');

        });

        Route::group(['prefix' => 'international'],function(){
            Route::get('/', 'adminPanel\UniversitiesController@int_index')->name('university-int');
            Route::get('/create', 'adminPanel\UniversitiesController@int_create')->name('university-int-create');
            Route::get('/edit/{id}', 'adminPanel\UniversitiesController@int_edit')->name('university-int-edit');
            Route::get('/view/{id}','adminPanel\UniversitiesController@show')->name('university-international-show');

        });

        Route::group(['prefix' => 'college'], function(){
            Route::get('/', 'adminPanel\UniversitiesController@ac_index')->name('university-college');
            Route::get('/create','adminPanel\UniversitiesController@ac_create')->name('university-college-create');
            Route::get('/edit/{id}', 'adminPanel\UniversitiesController@ac_edit')->name('university-college-edit');
            Route::get('/view/{id}','adminPanel\UniversitiesController@show')->name('university-college-show');


        });

        Route::post('/store', 'adminPanel\UniversitiesController@store')->name('university-store');
        Route::post('/update/{id}', 'adminPanel\UniversitiesController@update')->name('university-update');
        Route::get('/destroy/{id}', 'adminPanel\UniversitiesController@destroy')->name('university-destroy');
        Route::get('/status/{id}','adminPanel\UniversitiesController@status')->name('university-status');
        Route::post('/department/store', 'adminPanel\DepartmentController@store')->name('department-store');
        Route::get('/department/show/{id}', 'adminPanel\DepartmentController@show')->name('department-show');
        Route::get('/department/edit/{id}', 'adminPanel\DepartmentController@edit')->name('department-edit');
        Route::post('/department/update/{id}', 'adminPanel\DepartmentController@update')->name('department-update');
        Route::get('/department/destroy/{id}', 'adminPanel\DepartmentController@destroy')->name('department-destroy');



    });

    Route::group(['prefix' => 'events'], function(){
//        Route::resource('events','adminPanel\EventsController');
        Route::get('/pakistan', 'adminPanel\EventsController@p_index')->name('event-pak');
        Route::post('events/store','adminPanel\EventsController@store')->name('events.store');
        Route::post('events/update/{id}','adminPanel\EventsController@update')->name('events.update');
        Route::get('/pakistan/create', 'adminPanel\EventsController@p_create')->name('event-pak-create');
        Route::get('pakistan/show/{id}', 'adminPanel\EventsController@show')->name('event-show');
        Route::get('/pakistan/edit/{id}', 'adminPanel\EventsController@p_edit')->name('event-pak-edit');
        Route::get('/destroy.{id}', 'adminPanel\EventsController@destroy')->name('event-destroy');

        Route::get('/international','adminPanel\EventsController@i_index')->name('event-int');
        Route::get('/international/create', 'adminPanel\EventsController@i_create')->name('event-int-create');
        Route::get('/international/edit/{id}', 'adminPanel\EventsController@i_edit')->name('event-int-edit');


    });

    Route::group(['prefix' => 'scholarships'], function(){
        Route::get('/pakistan', 'adminPanel\ScholarshipsController@pak_index')->name('scholarships-pak');
        Route::get('/pakistan/create', 'adminPanel\ScholarshipsController@pak_create')->name('scholarship-pak-create');
        Route::post('pakistan/store', 'adminPanel\ScholarshipsController@store')->name('scholarships.store');
        Route::post('pakistan/update/{id}','adminPanel\ScholarshipsController@update')->name('scholarships.update');
        Route::get('/pakistan/edit/{id}', 'adminPanel\ScholarshipsController@p_edit')->name('scholarship-pak-edit');
        Route::get('/destroy/{id}', 'adminPanel\ScholarshipsController@destroy')->name('scholarship-destroy');
        Route::get('/show/{id}', 'adminPanel\ScholarshipsController@show')->name('scholarship-show');
        //This route is used to store data into database
        Route::get('/international', 'adminPanel\ScholarshipsController@int_index')->name('scholarship-int');
        Route::get('/international/create', 'adminPanel\ScholarshipsController@int_create')->name('scholarship-int-create');
        Route::get('/international/edit/{id}', 'adminPanel\ScholarshipsController@i_edit')->name('scholarship-int-edit');
    });

    Route::group(['prefix' => 'slider'], function(){
        Route::get('/', 'adminPanel\SlidersController@index')->name('slider');
        Route::get('/create', 'adminPanel\SlidersController@create')->name('slider-create');
        Route::post('/store', 'adminPanel\SlidersController@store')->name('slider-store');
        Route::get('/show/{id}', 'adminPanel\SlidersController@show')->name('slider-show');
        Route::post('/update/{id}', 'adminPanel\SlidersController@update')->name('slider-update');
        Route::get('/edit/{id}', 'adminPanel\SlidersController@edit')->name('slider-edit');
        Route::get('/destroy/{id}', 'adminPanel\SlidersController@destroy')->name('slider-destroy');
        Route::get('/status/{id}', 'adminPanel\SlidersController@status')->name('slider-status');
    });

    Route::group(['prefix' => 'banner'], function(){
        Route::get('/', 'adminPanel\BannersController@index')->name('banner');
        Route::get('/create', 'adminPanel\BannersController@create')->name('banner-create');
        Route::post('/store', 'adminPanel\BannersController@store')->name('banner-store');
        Route::get('/show/{id}', 'adminPanel\BannersController@show')->name('banner-show');
        Route::get('/edit/{id}', 'adminPanel\BannersController@edit')->name('banner-edit');
        Route::post('/update/{id}', 'adminPanel\BannersController@update')->name('banner-update');
        Route::get('/destroy/{id}', 'adminPanel\BannersController@destroy')->name('banner-destroy');
        Route::get('/search', 'adminPanel\BannersController@search')->name('banner-search');
    });

    Route::group(['prefix' => 'about'], function(){
        Route::get('/','adminPanel\AboutsController@index')->name('about');
        Route::get('/create', 'adminPanel\AboutsController@create')->name('about-create');
        Route::post('/store', 'adminPanel\AboutsController@store')->name('about-store');
        Route::get('/show/{id}', 'adminPanel\AboutsController@show')->name('about-show');
        Route::get('/edit/{id}', 'adminPanel\AboutsController@edit')->name('about-edit');
        Route::post('/update/{id}', 'adminPanel\AboutsController@update')->name('about-update');
        Route::get('/destroy/{id}', 'adminPanel\AboutsController@destroy')->name('about-destroy');
        Route::get('/status/{id}','adminPanel\AboutsController@status')->name('about-status');
    });

    Route::group(['prefix'=>'course'], function(){
        Route::get('/','adminPanel\CoursesController@index')->name('course');
        Route::get('/create', 'adminPanel\CoursesController@create')->name('course-create');
        Route::post('/store', 'adminPanel\CoursesController@store')->name('course-store');
        Route::get('/show/{id}', 'adminPanel\CoursesController@show')->name('course-show');
        Route::get('/edit/{id}', 'adminPanel\CoursesController@edit')->name('course-edit');
        Route::post('/update/{id}', 'adminPanel\CoursesController@update')->name('course-update');
        Route::get('/destroy/{id}', 'adminPanel\CoursesController@destroy')->name('course-destroy');
    });

    Route::group(['prefix'=>'user'], function(){
       Route::get('/', 'adminPanel\UsersController@index')->name('user');
       Route::get('/create', 'adminPanel\UsersController@create')->name('user-create');
       Route::get('/store', 'adminPanel\UsersController@store')->name('user-store');
       Route::get('/show/{id}','adminPanel\UsersController@show')->name('user-show');
       Route::get('/edit/{id}', 'adminPanel\UsersController#@edit')->name('user-edit');
       Route::get('/update/{id}', 'adminPanel\UsersController@update')->name('user-update');
       Route::get('/destroy/{id}', 'adminPanel\UsersController@destroy')->name('user-destroy');
    });

    Route::group(['prefix'=>'service'], function(){
        Route::get('/', 'adminPanel\ServicesController@index')->name('service');
        Route::get('/create', 'adminPanel\ServicesController@create')->name('service-create');
        Route::get('/store', 'adminPanel\ServicesController@store')->name('service-store');
        Route::get('/show/{id}','adminPanel\ServicesController@show')->name('service-show');
        Route::get('/edit/{id}', 'adminPanel\ServicesController#@edit')->name('service-edit');
        Route::get('/update/{id}', 'adminPanel\ServicesController@update')->name('service-update');
        Route::get('/destroy/{id}', 'adminPanel\ServicesController@destroy')->name('service-destroy');
    });

    Route::group(['prefix' => 'department'],function (){
        Route::get('/create/{parent}','adminPanel\DepartmentController@create')->name('department-create');
    });



});

\Illuminate\Support\Facades\Auth::routes();
