let baseurl = $('#baseurl').val();

$('.change_status').click(function () {
    let id= $(this).data('id');
    $.get({
        url:baseurl+'/record_status/'+id,
        success:function (data) {
            console.log(data);
            if(data == 1)
                $('.status'+id).html('<span class="badge badge-primary p-2">ENABLED</span>');
            if(data == 0)
                $('.status'+id).html('<span class="badge badge-danger p-2">DISABLE</span>');
        }
    })
});

$(document).ready(function () {
    $('.selectdrop').prepend('<option value="0">--Please Select One--</option>').selectpicker({
        liveSearch:true,
        showTick:true,
        title:"Please Select One",
        style:'btn select-with-transition',
        size:5
    });

});

function setFormValidation(id){
    $(id).validate({
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
        },
        errorPlacement : function(error, element) {
            $(element).append(error);
        },
    });
}

$(document).ready(function(){
    setFormValidation('#RegisterValidation');
    setFormValidation('#TypeValidation');
    setFormValidation('#LoginValidation');
    setFormValidation('#RangeValidation');
});

$('.wrapper-full-page').height($(window).height());

$(function () {
    tinymce.init({
        selector: ".tinymce",
        theme: "modern",
        menubar: "edit",
        setup: function (ed) {
            ed.on('keyup', function(e) {
                var content = ed.getContent().replace(
                    /(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|(\s+)/ig, '' );
                var max = 200;
                var len = content.length;
                if ( len  >= max ){
                    tinymce.dom.Event.cancel(e);
                }
            });

        },
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "emoticons template paste textcolor colorpicker textpattern imagetools nonbreaking"
        ],
        //toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar: "undo redo | bold italic | bullist numlist ",
        //toolbar2: "print preview media | forecolor backcolor emoticons",
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });
});


$(function () {
    tinymce.init({
        selector: ".tinymce_advance",
        theme: "modern",
        setup: function (ed) {
            ed.on('keyup', function(e) {
                var content = ed.getContent().replace(
                    /(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|(\s+)/ig, '' );
                var max = 200;
                var len = content.length;
                if ( len  >= max ){
                    tinymce.dom.Event.cancel(e);
                }
            });
        },
        plugins : ["advlist autolink lists link charmap image print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"],
        images_upload_url: 'http://localhost/tourily/public/pageImagesUpload',

        // override default upload handler to simulate successful upload
        images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;

            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', 'http://localhost/tourily/public/pageImagesUpload');

            xhr.onload = function() {
                var json;

                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
                console.log(xhr.responseText);
                json = JSON.parse(xhr.responseText);

                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }

                success(json.location);
            };

            formData = new FormData();
            formData.append('files[]', blobInfo.blob(), blobInfo.filename());
            formData.append('_token', $('#token').val());

            xhr.send(formData);
        },
    });
});

$('.file-input').fileinput({
    showUpload: false,
    theme:'fas',
    showZoom:false,
    layoutTemplates : {'footer':''},
    previewFileIconSettings: {
        'docx': '<i class="fa fa-file-word text-primary"></i>',
        'xlsx': '<i class="fa fa-file-excel text-success"></i>',
        'pptx': '<i class="fa fa-file-powerpoint text-danger"></i>',
        'pdf': '<i class="fa fa-file-pdf text-danger"></i>',
        'zip': '<i class="fa fa-file-archive text-muted"></i>',
    }
});